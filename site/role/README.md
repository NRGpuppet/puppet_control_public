role
====

####Table of Contents

1. [Overview - What is the Role module?](#overview)
2. [Usage - The classes and parameters available for configuration](#usage)
3. [Advanced Usage - Advanced use cases, e.g. deploying a Puppetserver](#advanced-usage)

Overview
--------

The Role module includes classes needed for bootstrapping and provisioning machines
from this control repo.  For example, this module includes a basic role class shared by
all machines, role classes related to deploying a Puppetserver, and additional role
classes for those applications to be managed by Puppet.

This module is intended to be used in conjunction with a companion Profile module, whereby 
nodes will include classes in this Role module that include includes classes in the
Profile module.  This seperation allows classes in this Role module to take more human-readable
names, e.g. Maven server, build server, load balancer, while classes in the Profile
module focus more on the layers of the actual application stack, e.g. nginx server, JVM,
postgresql server.

Additionally, this separation allows for ease in changing and editing application layers.
For example, switching from Apache to Nginx could be done by editing a single line in a
Role module class, instead of editing dozens of node manifests for all the machines assigned
to that role.

Here are the conventions preferred for the Profile and Role modules:

1. Classes in the Role module only include classes in the Profile module.  Classes from
additional modules would be included in Profile module classes.
1. The Role module takes no parameters, only the Profile module.  Likewise, only classes
in the Profile module do hiera lookups.
1. A slightly different role should be a different role class entirely.  E.g.
role::puppet::puppetserver_int_db and role::puppet::puppetserver_ext_db.

Please also refer to the Profile module [README](../profile/README.md).

Finally, cf. [The origins of roles and profiles](http://www.craigdunn.org/2012/05/239/)

Usage
-----

Note that all machines will include this module's base role class init.pp via the 
environment-wide manifests/site.pp.  So, any classes included via hiera or puppet
code would be in addition to the base role.

Example in hiera:

```
classes:
 - role::xnat::xnat
 - ... additional classes

# Puppet module params for puppet-agent
puppet::runmode: service
puppet::puppetmaster: (FQDN of master Puppetserver)
puppet::ca_server: (FQDN of Puppetserver CA, if not the master Puppetserver)

# Hiera params for XNAT module
xnat::db_name: xnatdb
xnat::db_user: xnatuser
xnat::db_host: localhost
xnat::db_pass: secure_passwod

# Hiera params for other modules
...
```

## Classes included in this module:

### role
Base role class shared by all machines.  All roles below will include this class, and
presently they do via environment-wide manifests/site.pp.

### XNAT-related roles

#### role::xnat::xnat
This role deploys an XNAT instance, including SSL certificate. More details TBD.

### Puppet-related roles

#### role::puppet::puppetserver_int_db
This role deploys a Puppetserver with onboard PuppetDB.

#### role::puppet::puppetserver_ext_db
This role deploys a Puppetserver with expectation of an external PuppetDB host coming online later.

#### role::puppet::puppetca
This role deploys a Puppetserver acting as dedicated CA, i.e. as companion to another node running
one of the role::puppet::puppetserver* roles above.

#### role::puppet::puppetdb
This role deploys a PuppetDB host, i.e. to be used in conjunction with a node running the
role::puppet::puppetserver_ext_db role above.

### Monitoring roles

#### role::monitoring::icinga2_server
Installs the Icgina2 monitoring server and its web UI.  Still a WIP.

Advanced Usage
--------------

## Provisioning machines for alternate environments

How to provision machines for an environment besides the active one.  TBD.

## Provisioning a Puppetserver

To provision a Puppetserver, there are several options, with each requiring a different
constellation of classes from this module.  Besides the puppetserver deployment options
enumerated below, do also refer to the [provision scripts](../../provision) for more details
about bootstapping a puppetserver and puppet nodes from scratch.

### Single machine housing Puppserver, CA, and PuppetDB

This is the simplest option for production use.  The hiera code below demonstrates the
classes to include and parameters to define to support this option.  That is, the machine
in question should resolve these classes and params via hiera during bootstrap, with the 
"role::puppet::puppetserver_int_db" class being unique to this machine.

```
classes:
 - role::puppet::puppetserver_int_db

# Profile module puppet params
profile::puppet::puppetserver::delayed_puppet_runmode: service
profile::puppet::puppetserver::delayed_server_reports: 'store,puppetdb'
profile::puppet::puppetserver::delayed_server_storeconfigs_backend: puppetdb
profile::puppet::puppetserver::delayed_server_puppetdb_host: (this machine's FQDN)
profile::puppet::puppetserver::server_autosign_nodes:
  - "*.domain1.com"
  - "somehost.domain2.com"
profile::puppet::r10k::r10k_remote_poll:
  hour: "*"
profile::puppet::r10k::r10k_remote_poll_random_minute: true
profile::puppet::hiera::create_keys: true

# R10k module params for puppetserver
r10k::remote: "ssh://hg@bitbucket.org/NRGpuppet/puppet_control_public"

# Puppet module params for puppetserver and puppet-agent
puppet::puppetmaster: (this machine's FQDN)
puppet::runmode: service
puppet::server_jvm_max_heap_size: 2G
puppet::server_jvm_min_heap_size: 1G
puppet::server_foreman: false
puppet::server_reports: store
puppet::server_common_modules_path: /etc/puppetlabs/code/common_modules
puppet::hiera_config: /etc/puppetlabs/code/hiera.yaml

# PuppetDB module params
puppetdb::manage_package_repo: true
puppetdb::postgres_version: "9.4"
puppetdb::postgres_datadir: /pgdata
puppetdb::database_password: areallygoodsecret

# Java module params
java::distribution: jdk
```

### One machine housing Puppserver + CA, another the PuppetDB

This option has the PuppetDB host residing on a separate machine, which the Puppetserver
would treat as an agent node.  To resolve dependency issues at bootstrapping, the 
profile::puppet::puppetserver class will first deploy the embedded PuppetDB locally, and
then point the Puppetserver to the external PuppetDB host once it becomes available (as
announced via exported resource).

Classes and parameters in hiera for the Puppetserver+CA machine, e.g. **puppetmaster.domain.com**:
```
classes:
 - role::puppet::puppetserver_ext_db

# Profile module puppet params
profile::puppet::puppetserver::delayed_puppet_runmode: service
profile::puppet::puppetserver::delayed_server_reports: 'store,puppetdb'
profile::puppet::puppetserver::delayed_server_storeconfigs_backend: puppetdb
profile::puppet::puppetserver::delayed_server_puppetdb_host: puppetdb.domain.com
profile::puppet::puppetserver::server_autosign_nodes:
  - "*.domain1.com"
  - "somehost.domain2.com"
profile::puppet::r10k::r10k_remote_poll:
  hour: "*"
profile::puppet::r10k::r10k_remote_poll_random_minute: true
profile::puppet::hiera::create_keys: true

# R10k module params for puppetserver
r10k::remote: "ssh://hg@bitbucket.org/NRGpuppet/puppet_control_public"

# Puppet module params for puppetserver and puppet-agent
puppet::puppetmaster: puppetmaster.domain.com
puppet::runmode: service
puppet::server_jvm_max_heap_size: 2G
puppet::server_jvm_min_heap_size: 1G
puppet::server_foreman: false
puppet::server_reports: store
puppet::server_common_modules_path: /etc/puppetlabs/code/common_modules
puppet::hiera_config: /etc/puppetlabs/code/hiera.yaml

# Java module params
java::distribution: jdk
```

Classes and parameters in hiera for the PuppetDB host machine, e.g. **puppetdb.domain.com**:
```
classes:
 - role::puppet::puppetdb

# Puppet module params for puppet-agent
puppet::puppetmaster: puppetmaster.domain.com
puppet::runmode: service

# PuppetDB module params
puppetdb::manage_package_repo: true
puppetdb::postgres_version: "9.4"
puppetdb::postgres_datadir: /pgdata
puppetdb::database_password: areallygoodsecret

# Java module params
java::distribution: jdk
```

### One machine housing the CA, another the housing the Puppetserver + PuppetDB

This option has the CA residing on a separate machine, with the master Puppetserver and PuppetDB
residing on a 2nd machine.  That is, both machines will run the JVM-based puppetserver, with the
first acting exclusively as CA and the 2nd as master puppetserver.  This option may be necessary
for environments with special requirements for the Certificate Authority, eg. the CA must use
a PKI infrastructure provided by an HSM appliance.

For bootstrap dependency issues, the 1st machine housing the CA must be up and running before
bringing up the 2nd machine housing the master Puppetserver.

Classes and parameters in hiera for the CA machine, e.g. **puppetca.domain.com**:
```
classes:
 - role::puppet::puppetca

#  Profile module puppet params 
profile::puppet::puppetca::delayed_puppet_runmode: service
profile::puppet::puppetca::server_autosign_nodes:
  - "*.domain1.com"
  - "somehost.domain2.com"
profile::puppet::hiera::create_keys: true

# Puppet module params for puppet-agent and puppetserver
puppet::puppetmaster: puppetmaster.domain.com
puppet::ca_server: puppetca.domain.com
puppet::server_ca: true
puppet::runmode: service
puppet::server_jvm_max_heap_size: 512M
puppet::server_jvm_min_heap_size: 256M
puppet::server_foreman: false
puppet::server_reports: store
puppet::server_common_modules_path: /etc/puppetlabs/code/common_modules
puppet::hiera_config: /etc/puppetlabs/code/hiera.yaml

# Java module params
java::distribution: jdk
```

Classes and parameters in hiera for the Puppetmaster+DB machine, e.g. **puppetmaster.domain.com**:
```
classes:
 - role::puppet::puppetserver_int_db

# Profile module puppet params
profile::puppet::puppetserver::delayed_puppet_runmode: service
profile::puppet::puppetserver::delayed_server_reports: 'store,puppetdb'
profile::puppet::puppetserver::delayed_server_storeconfigs_backend: puppetdb
profile::puppet::puppetserver::delayed_server_puppetdb_host: puppetmaster.domain.com
profile::puppet::r10k::r10k_remote_poll:
  hour: "*"
profile::puppet::r10k::r10k_remote_poll_random_minute: true
profile::puppet::hiera::create_keys: true

# R10k module params for puppetserver
r10k::remote: "ssh://hg@bitbucket.org/NRGpuppet/puppet_control_public"

# Puppet module params for puppetserver and puppet-agent
puppet::puppetmaster: puppetmaster.domain.com
puppet::ca_server: puppetca.domain.com
puppet::server_ca: false
puppet::runmode: service
puppet::server_jvm_max_heap_size: 2G
puppet::server_jvm_min_heap_size: 1G
puppet::server_foreman: false
puppet::server_reports: store
puppet::server_common_modules_path: /etc/puppetlabs/code/common_modules
puppet::hiera_config: /etc/puppetlabs/code/hiera.yaml

# PuppetDB module params
puppetdb::manage_package_repo: true
puppetdb::postgres_version: "9.4"
puppetdb::postgres_datadir: /pgdata
puppetdb::database_password: areallygoodsecret

# Java module params
java::distribution: jdk
```

### The CA, the Puppetserver, and the PuppetDB each on separate machines

This option has the CA, the master Puppetserver, and the PuppetDB host each residing on 3 separate
machines.  This option provides the greatest flexibility in scaling, but also the most complexity.

For bootstrap dependency issues, the 1st machine housing the CA must be up and running first, then
the 2nd machine housing the master Puppetserver, and finally the 3rd machine housing the PuppetDB.

Classes and parameters in hiera for the CA machine, e.g. **puppetca.domain.com**:
```
classes:
 - role::puppet::puppetca

#  Profile module puppet params 
profile::puppet::puppetca::delayed_puppet_runmode: service
profile::puppet::puppetca::server_autosign_nodes:
  - "*.domain1.com"
  - "somehost.domain2.com"
profile::puppet::hiera::create_keys: true

# Puppet module params for puppet-agent and puppetserver
puppet::puppetmaster: puppetmaster.domain.com
puppet::ca_server: puppetca.domain.com
puppet::server_ca: true
puppet::runmode: service
puppet::server_jvm_max_heap_size: 512M
puppet::server_jvm_min_heap_size: 256M
puppet::server_foreman: false
puppet::server_reports: store
puppet::server_common_modules_path: /etc/puppetlabs/code/common_modules
puppet::hiera_config: /etc/puppetlabs/code/hiera.yaml

# Java module params
java::distribution: jdk
```

Classes and parameters in hiera for the Puppetserver machine, e.g. **puppetmaster.domain.com**:
```
classes:
 - role::puppet::puppetserver_ext_db

# Profile module puppet params
profile::puppet::puppetserver::delayed_puppet_runmode: service
profile::puppet::puppetserver::delayed_server_reports: 'store,puppetdb'
profile::puppet::puppetserver::delayed_server_storeconfigs_backend: puppetdb
profile::puppet::puppetserver::delayed_server_puppetdb_host: puppetdb.domain.com
profile::puppet::r10k::r10k_remote_poll:
  hour: "*"
profile::puppet::r10k::r10k_remote_poll_random_minute: true
profile::puppet::hiera::create_keys: true

# R10k module params for puppetserver
r10k::remote: "ssh://hg@bitbucket.org/NRGpuppet/puppet_control_public"

# Puppet module params for puppetserver and puppet-agent
puppet::puppetmaster: puppetmaster.domain.com
puppet::ca_server: puppetca.domain.com
puppet::runmode: service
puppet::server_ca: false
puppet::server_jvm_max_heap_size: 2G
puppet::server_jvm_min_heap_size: 1G
puppet::server_foreman: false
puppet::server_reports: store
puppet::server_common_modules_path: /etc/puppetlabs/code/common_modules
puppet::hiera_config: /etc/puppetlabs/code/hiera.yaml

# Java module params
java::distribution: jdk
```

Classes and parameters in hiera for the PuppetDB host machine, e.g. **puppetdb.domain.com**:
```
classes:
 - role::puppet::puppetdb

# Puppet module params for puppet-agent
puppet::puppetmaster: puppetmaster.domain.com
puppet::ca_server: puppetca.domain.com
puppet::runmode: service

# PuppetDB module params
puppetdb::manage_package_repo: true
puppetdb::postgres_version: "9.4"
puppetdb::postgres_datadir: /pgdata
puppetdb::database_password: areallygoodsecret

# Java module params
java::distribution: jdk
```

### A single machine housing the CA, Puppetserver, and embedded PuppetDB

This arrangement uses the deprecated, embedded puppetDB.  Since that database option
doesn't provide robust backup support, it is not suitable for production use, but it may
be useful for testing with a very small number of puppet nodes (i.e. under Vagrant).  This
option can be enabled by pointing a machine running the role::puppetserver_ext_db class to
a fictitious external PuppetDB host.

This option actually takes avantage of the profile::puppet::puppetserver class deploying the
embedded puppetDB locally in anticipation of an external PuppetDB host becoming available
later.  Since the external puppetDB host is fictitious, the embedded puppetDB will remain
in operation.

```
classes:
 - role::puppet::puppetserver_ext_db

# Profile module puppet params
profile::puppet::puppetserver::delayed_puppet_runmode: service
profile::puppet::puppetserver::delayed_server_reports: 'store,puppetdb'
profile::puppet::puppetserver::delayed_server_storeconfigs_backend: puppetdb
profile::puppet::puppetserver::delayed_server_puppetdb_host: fakehost.fakedomain.com
profile::puppet::puppetserver::server_autosign_nodes:
  - "*.domain1.com"
  - "somehost.domain2.com"
profile::puppet::r10k::r10k_remote_poll:
  hour: "*"
profile::puppet::r10k::r10k_remote_poll_random_minute: true
profile::puppet::hiera::create_keys: true

# R10k module params for puppetserver
r10k::remote: "ssh://hg@bitbucket.org/NRGpuppet/puppet_control_public"

# Puppet module params for puppetserver and puppet-agent
puppet::puppetmaster: (this machine's FQDN)
puppet::runmode: service
puppet::server_jvm_max_heap_size: 2G
puppet::server_jvm_min_heap_size: 1G
puppet::server_foreman: false
puppet::server_reports: store
puppet::server_common_modules_path: /etc/puppetlabs/code/common_modules
puppet::hiera_config: /etc/puppetlabs/code/hiera.yaml

# Java module params
java::distribution: jdk
```


