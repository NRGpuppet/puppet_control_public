# This role installs the Icinga v2 server along with its web 2 GUI
#
class role::monitoring::icinga2_server {
  include profile::db::postgresql_server
  include profile::monitoring::icinga2_server
  include profile::ssl_certs
  include profile::monitoring::icingaweb2
}
