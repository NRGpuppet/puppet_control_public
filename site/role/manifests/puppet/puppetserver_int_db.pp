# This is the simplest Puppetserver option for production use. This has the JVM-based
# Puppetserver and the PostgreSQL-backed PuppetDB all on one machine.
#
class role::puppet::puppetserver_int_db {
  include profile::db::postgresql_server
  include profile::puppet::puppetserver
  include profile::puppet::r10k
  include profile::puppet::hiera
  include profile::puppet::puppetdb
}
