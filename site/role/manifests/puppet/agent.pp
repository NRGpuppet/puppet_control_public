# This is the role class for the puppent agent.  This class is intended to be run last.
# Presently this class is included via environment-wide manifests/site.pp.
#
class role::puppet::agent {
  include profile::puppet::agent
}
