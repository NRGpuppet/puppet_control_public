# This is the role for a Puppetserver CA, i.e. to ask as companion to a
# Puppetserver master.
#
class role::puppet::puppetca {
  include profile::puppet::puppetca
}
