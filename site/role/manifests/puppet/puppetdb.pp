# This is the role for a PuppetDB host.
#
class role::puppet::puppetdb {
  include profile::db::postgresql_server
  include profile::puppet::puppetdb
}
