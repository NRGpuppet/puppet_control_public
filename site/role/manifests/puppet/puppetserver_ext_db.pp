# This role represents a JVM-based Puppetserver, but with an external
# PuppetDB host.  To resolve dependency issues at bootstrapping, the
# profile::puppet::puppetserver class will first deploy the embedded PuppetDB
# locally, and then point the Puppetserver to the external PuppetDB host once
# it becomes available (as announced via exported resource).
#
class role::puppet::puppetserver_ext_db {
  include profile::puppet::puppetserver
  include profile::puppet::r10k
  include profile::puppet::hiera
}
