# This is the role for an OmniOS ZFS Server.
#
class role::zfs::zfs_server_ha {
  include nrgzfs
  include nrgzfs::ha
}

