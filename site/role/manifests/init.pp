# This manifest is the basic role class for this environment.  All nodes will include
# this class first (or near first), and presently this class is included via environment-wide
# manifests/site.pp.
#
# Also, note that configuration which is OS-specific yet reasonably independent of NRG's
# applications or projects is already handled by custom modules like nrg/nrgcentos and
# nrg/illumos.
#
class role {
  include profile::base
}
