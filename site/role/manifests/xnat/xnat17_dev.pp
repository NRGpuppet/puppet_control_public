# This is the role for an XNAT v1.7 dev instance.
#
class role::xnat::xnat17_dev {
  include ::profile::db::postgresql_server
  include ::profile::ssl_certs
  include ::profile::pkg_repos
  class { '::xnat': }
  Class['Profile::Pkg_repos'] -> Class['Xnat']
}
