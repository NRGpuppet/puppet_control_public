# This is the role for a basic XNAT instance.  Note it also includes
# profile::ssl_certs to deploy an SSL certificate.
#
class role::xnat::xnat {
  include profile::ssl_certs
  include xnat
}
