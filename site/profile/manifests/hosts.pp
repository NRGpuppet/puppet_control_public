# == Class: profile::hosts
#
# This manifest is a wrapper class for the ghoneycutt/hosts module.  This class exists solely
# to permit deeper merging on hiera() lookups of the 'host_entries' hash, which it
# will then pass to the hosts module.
#
# That is, this class is intended to permit specifying and overriding that hash at
# multuple levels of the hiera hierarcy.
#
# The default 'priority lookup' behavior by Puppet's automated parameter lookup is just
# inadequate for this instance, as any overriding definition of the hash hosts::host_entries,
# i.e. at a higher level of hierarchy, would not merge with non-overlapping items in that
# same hash (e.g. other /etc/hosts entries) defined at lower levels.  This default behavior is
# badly counter-intuitive, and so this class seeks to work-around Puppetlabs' willful obstinance
# on the subject.
#
# Cf. https://docs.puppetlabs.com/hiera/3.0/puppet.html#automatic-parameter-lookup
#
# More info about this class in the profile module README.md
#
# === Parameters
#
# NONE
#
# === Variables
#
# Below are variables this class will look up in hiera().
#
# *profile::hosts::host_entries*
#  A hash of /etc/hosts entries to manage, to be passed to the ghoneycutt/hosts module
#
# === Examples
#
#  class { '::profile::hosts': }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::hosts {

  # Retrieve hashes from hiera using deep merging behavior
  $host_entries = hiera_hash('profile::hosts::host_entries', undef)

  # Now invoke hosts module.  Other params can be passed to it via hiera.
  class { '::hosts':
    host_entries => $host_entries,
  }
}
