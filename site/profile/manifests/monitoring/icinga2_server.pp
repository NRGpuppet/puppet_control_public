# == Class: profile::monitoring::icinga2_server
#
# This class is a wrapper around the icinga2 module, installing the Icinga v2 server.  This
# class assumes a PostgreSQL database is provided in the parameters.  Note this class is safe
# to include alongside profile::monitoring::icinga2_client.
#
# This class expects PostgreSQL credentials in its parameters so that it can verify database
# connectivity before installing Icinga2 server.  Note this class does NOT deploy PostgreSQL
# server, the role, or the database.  That can be done by profile::db::postgresql_server class.
#
# More info about this class in the profile module README.md
# 
# === Parameters
#
# Parameters this class accepts.
#
# [*db_pass*]
#   Required, the PostgreSQL password.
#
# [*db_user*]
#   The PostgreSQL user, default 'icinga2'
#
# [*db_name*]
#   The PostgreSQL database name, default 'icinga2_data'
#
# [*db_host*]
#   The PostgreSQL host, default 'localhost'
#
# [*db_port*]
#   The PostgreSQL port, default 5432
#
# [*hostgroups*]
#   Hash of Icinga2 host groups for create_resources('icinga2::object::hostgroup', $hostgroups).
#   See defaults in code below.
#
# [*servicegroups*]
#   Hash of service groups for create_resources('icinga2::object::servicegroup', $servicegroups).
#   See defaults in code below.
#
# [*apply_services*]
#   Hash of objects for create_resources('icinga2::object::apply_service', $apply_services).
#   See defaults in code below.
#
# [*services*]
#   Optional, hash of services for create_resources('icinga2::object::service', $services).
#   Default undef.
#
# [*hosts*]
#   Optional, hash of hosts for create_resources('icinga2::object::host', $hosts).
#   Any hosts defined here shouldn't collide with exported host definitions collected by
#   this class.  I.e., just use this hash to define hosts that won't have a local Icinga2
#   client deployed by profile::monitoring::icinga2_client.  Default undef.
#
# [*debuglog*]
#   Whether to enable the debuglog feature of Icinga2 server. Default false.
#
# [*graphite_host*]
#   The FQDN of the Graphite/InfluxDB host, default false (= no Graphite host).
#
# [*graphite_port*]
#   The Graphite/InfluxDB post, default 2003
#
# === Variables
#
# Custom variables used by this class.
#
# NONE
#
# === Examples
#
#  class { '::profile::monitoring::icinga2_server':
#    db_pass => 'areallygoodsecret',
#    db_host => 'localhost',
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::monitoring::icinga2_server (
  String $db_pass,
  String $db_user = 'icinga2',
  String $db_host = 'localhost',
  String $db_name = 'icinga2_data',
  Variant $db_port = 5432,
  Hash $hostgroups = {
    'linux-servers' => {
      display_name => 'Linux Servers',
      assign_where => 'host.vars.os == "Linux"',
    },
    'windows-servers' => {
      display_name => 'Windows Servers',
      assign_where => 'host.vars.os == "Windows"',
    }
  },
  Hash $servicegroups = {
    'ping' => {
      display_name => 'Ping Checks',
      assign_where => 'match("ping*", service.name)',
    },
    'http' => {
      display_name => 'HTTP Checks',
      assign_where => 'match("http*", service.check_command)',
    },
    'disk' => {
      display_name => 'Disk Checks',
      assign_where => 'match("disk*", service.check_command)',
    },
  },
  Hash $apply_services = {
    'ping4' => {
      check_command => 'ping4',
      assign_where => 'host.address'
    },
    'ping6' => {
      check_command => 'ping6',
      assign_where => 'host.address6',
    },
    'ssh' => {
      check_command => 'ssh',
      assign_where => '(host.address || host.address6) && host.vars.os == "Linux"',
    },
    'icinga' => {
      check_command => 'icinga',
      assign_where => 'host.name == NodeName',
    },
    'users' => {
      check_command => 'users',
      command_endpoint => 'host.vars.remote_client',
      custom_append => [ 'vars.users_wgreater = host.vars.users_wgreater',
                         'vars.users_cgreater = host.vars.users_cgreater',
                         ],
      assign_where => 'host.vars.remote_client',
    },
    'disk' => {
      check_command => 'disk',
      command_endpoint => 'host.vars.remote_client',
      custom_append => [ 'vars.disk_wfree = host.vars.disk_wfree',
                         'vars.disk_cfree = host.vars.disk_cfree',
                         ],
      assign_where => 'host.vars.remote_client',
    },
    'load' => {
      check_command => 'load',
      command_endpoint => 'host.vars.remote_client',
      assign_where => 'host.vars.remote_client',
    },
  },
  Variant $graphite_host = false,
  Variant $graphite_port = 2003,
  Boolean $debuglog = false,
  Optional[Hash] $services = undef,
  Optional[Hash] $hosts = undef,
) {
  
  include firewall
  include postgresql::client
  include ::profile::monitoring::client::icinga2_client_shared
  include ::profile::pkg_repos

  # Declare the appropriate Icinga2 class, based on OS
  case $::osfamily {
    /^(Debian|RedHat)$/ : {

      # Verify connectivity to database before proceeding
      # TODO: Revise to work with pgsql ident authentication (also must modify Icinga2 module)
      postgresql::validate_db_connection { "validate icinga2_server pgsql connection":
        database_host     => $db_host,
        database_port     => $db_port,
        database_username => $db_user,
        database_password => $db_pass,
        database_name     => $db_name,
      }
      ->

      # Install Icinga 2, with additional non-critical params specified in hiera.
      class { '::icinga2':
        db_type => 'pgsql',
        db_host => $db_host,
        db_port => $db_port,
        db_name => $db_name,
        db_user => $db_user,
        db_pass => $db_pass,
        manage_database => true,
        config_template => 'profile/monitoring/icinga2.conf.erb',
      }

      # RedHat OS's require EPEL for nagios plugins needed by icinga2
      Class['Profile::Pkg_repos'] -> Class['Icinga2']

      # Put our plugin dirs under Puppet control
      # TODO: Make less CentOS specific
      file_line {
        'icinga2_constants_comment':
          path => '/etc/icinga2/constants.conf',
          after => '^ \* the other configuration files.*$',
          line => ' * This file is managed by Puppet; please be careful editing.',
          require => Class['Icinga2'];
        'icinga2_constants_plugindir':
          path => '/etc/icinga2/constants.conf',
          match => '^const PluginDir =.*$',
          line => "const PluginDir = \"${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}\"",
          require => Class['Icinga2'];
        'icinga2_constants_plugincontribdir':
          path => '/etc/icinga2/constants.conf',
          match => '^const PluginContribDir =.*$',
          line => "const PluginContribDir = \"${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}\"",
          require => Class['Icinga2'];
        'icinga2_constants_manubulonplugindir':
          path => '/etc/icinga2/constants.conf',
          match => '^const ManubulonPluginDir =.*$',
          line => "const ManubulonPluginDir = \"${::profile::monitoring::client::icinga2_client_shared::manubulonplugindir}\"",
          require => Class['Icinga2'];
      }

      # Icinga2 install deposits default conf files that conflict with puppet, so remove them.
      # TODO: Revise to use $::icinga2::purge_confd = true, if needed.
      file {
        '/etc/icinga2/conf.d/hosts.conf':
          ensure => absent;
        '/etc/icinga2/conf.d/groups.conf':
          ensure => absent;
        '/etc/icinga2/conf.d/services.conf':
          ensure => absent;
      }

      # Enable additional, non-default Icinga2 features.  Note some of these have their own class.
      class { '::icinga2::feature::command': }
      icinga2::feature { 'livestatus':
        manage_file => false,
      }
      # Not sure why Icinga2 install doesn't create these files
      file {
        '/var/spool/icinga2/tmp/service-perfdata':
          ensure => file,
          owner => $::icinga2::config_owner,
          group => $::icinga2::config_group
          ;
        '/var/spool/icinga2/tmp/host-perfdata':
          ensure => file,
          owner => $::icinga2::config_owner,
          group => $::icinga2::config_group;
      }
      ->
      icinga2::feature { 'perfdata':
        manage_file => false,
      }
      if $graphite_host {
        class { '::icinga2::feature::graphite':
          host => $graphite_host,
          port => $graphite_port,
          enable_send_thresholds => true,
          enable_send_metadata => true,
        }
      }
      if $debuglog {
        icinga2::feature { 'debuglog':
          manage_file => false,
        }
      }
      class { '::icinga2::feature::syslog': }

      # Create Icinga2 API listener object locally, along with local zone that Icinga2 clients will
      # use as their master.  NO parent zone, since I'm the master.
      class { '::icinga2::pki::puppet': }
      class { '::icinga2::feature::api':
        accept_commands => true,
      }

      # Create the host groups, service groups, services, and service applys from parameters
      $resource_defaults = {
        target_file_owner => $::icinga2::config_owner,
        target_file_group => $::icinga2::config_group,
        target_file_mode => $::icinga2::config_mode,
      }
      create_resources('icinga2::object::hostgroup', $hostgroups, $resource_defaults)
      create_resources('icinga2::object::servicegroup', $servicegroups, $resource_defaults)
      create_resources('icinga2::object::apply_service', $apply_services, $resource_defaults)
      if $services {
        create_resources('icinga2::object::service', $services, $resource_defaults)
      }
      if $hosts {
        create_resources('icinga2::object::host', $hosts, $resource_defaults)
      }

      # Open port 5665 HTTP for incoming Icinga2 client.
      firewall { '100 allow Icinga2 client access':
        dport    => 5665,
        ctstate  => 'NEW',
        proto    => tcp,
        action   => accept,
        chain    => 'INPUT',
      }

      # Trigger explicit refresh of Icinga2 service, needed after first run for some reason
      Class['Icinga2::Feature::Api'] -> Firewall['100 allow Icinga2 client access']
      Firewall['100 allow Icinga2 client access'] ~> Service['icinga2']

    }
    default: {
      fail ( "Class profile::monitoring::icinga2_server not supported for ${::osfamily}")
    }
  }

  # Also include Icinga2 client class; server is itself a client.
  if (! defined(Class['Profile::Monitoring::Icinga2_client'])) {
    include ::profile::monitoring::icinga2_client
  }

  # Collect all @@icinga2::object::host and other resources exported by other machines
  Icinga2::Object::Host <<| tag == "icinga2" |>> {
    target_file_owner => $::icinga2::config_owner,
    target_file_group => $::icinga2::config_group,
    target_file_mode => $::icinga2::config_mode,
  }
  Icinga2::Object::Zone <<| tag == "icinga2" |>> {
    parent => "${::fqdn}",
  }
  Icinga2::Object::Apply_service <<| tag == "icinga2" |>> {
    target_file_owner => $::icinga2::config_owner,
    target_file_group => $::icinga2::config_group,
    target_file_mode => $::icinga2::config_mode,
  }

}
