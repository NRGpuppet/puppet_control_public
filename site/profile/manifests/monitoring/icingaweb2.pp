# == Class: profile::monitoring::icingaweb2
#
# This class is a wrapper around the icingaweb2 module, installing the Icinga v2 web UI and
# related components.  Specifically, this class will install MySQL server locally, as Icingaweb2
# install process requires that.  The Icingaweb2 UI will communicate with the Icinga2 server via
# IDO, given the connection information provided in the parameters.
#
# This class can be run on the same machine as profile::monitoring::icinga2_server, but
# it doesn't have to.
#
# Also note this class doesn't deploy SSL certificate/key/etc, rather profile::ssl_certs does that.
# This class just expects pointers to those files in its parameters.
#
# More info about this class in the profile module README.md
# 
# === Parameters
#
# Parameters this class accepts.
#
# [*mysql_root_password*]
#   Required, the root password for the MySQL server.  Needed for Icingaweb2 installation.
#
# [*ido_db*]
#
# [*ido_db_pass*]
#
# [*ido_db_user*]
#
# [*ido_db_host*]
#
# [*ido_db_port*]
#
# [*ido_db_name*]
#
# [*web_db_pass*]
#
# [*web_db_user*]
#
# [*web_db_name*]
#
# [*web_db_host*]
#
# [*web_db_port*]
#
# [*icingaadmin_pass*]
#
# [*web_ui_hostname*]
#
# [*web_ui_ssl_cert_file*]
#
# [*web_ui_ssl_key_file*]
#
# [*web_ui_ssl_chain_file*]
#
# [*web_ui_ssl_ca_file*]
#
# === Variables
#
# Custom variables used by this class.
#
# [*timezone::timezone*]
#   This class will include the Timezone module, and it needs to resolve a value for
#   $::timezone::timezone.
#
# === Examples
#
#  class { '::profile::monitoring::icingaweb2':
#    mysql_root_pass => 'areallygoodsecret',
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::monitoring::icingaweb2 (
  String $mysql_root_pass,
  String $ido_db = 'pgsql',
  String $ido_db_pass,
  String $ido_db_user = 'icinga2',
  String $ido_db_host,
  String $ido_db_name = 'icinga2_data',
  Variant $ido_db_port = 5432,
  String $web_db_pass,
  String $web_db_user = 'icingaweb2',
  String $web_db_host = 'localhost',
  String $web_db_name = 'icingaweb2_data',
  Variant $web_db_port = 3306,
  String $icingaadmin_pass,
  String $web_ui_hostname = $::fqdn,
  String $web_ui_ssl_cert_file,
  Variant $web_ui_ssl_key_file = false,
  Variant $web_ui_ssl_chain_file = false,
  Variant $web_ui_ssl_ca_file = false,
) {
  
  include firewall
  include timezone
  include ::profile::pkg_repos
  
  # Declare the appropriate Icinga2 class, based on OS
  case $::osfamily {
    /^(Debian|RedHat)$/ : {
      
      if ($::osfamily == 'RedHat') {
        # HTTPD also needs to SELinux permission to connect to DB over network
        exec { 'icingaweb2 enable httpd_can_network_connect_db':
          command => 'setsebool -P httpd_can_network_connect_db 1',
          onlyif => [ "getsebool httpd_can_network_connect_db | grep '> off'",
                      "getsebool httpd_can_network_connect_db | grep -v disabled" ]
        }
        Exec['icingaweb2 enable httpd_can_network_connect_db'] -> Class['Icingaweb2']
      }
      
      # Install MySQL server for Icinga2 web UI
      class { '::mysql::server':
        # This creates /root/.my.cnf needed by icingaweb2 install
        root_password => $mysql_root_pass,
      }
      mysql::db { $web_db_name:
        user     => $web_db_user,
        password => $web_db_pass,
        host     => $web_db_host,
        grant    => ['SELECT', 'UPDATE', 'INSERT', 'DELETE', 'DROP', 'CREATE VIEW', 'INDEX', 'EXECUTE'],
      }
      
      # Install Apache and PHP (no default vhost)
      class { '::apache':
        default_vhost => false,
      }
      apache::listen { '443': }
      apache::listen { '80': }
      class { '::apache::mod::php': }
      class { '::apache::mod::ssl': }

      # Set PHP timezone to quell Icingaweb2 complaint
      # TODO: Fix this so not CentOS specific
      if $::timezone::timezone {
        file_line { 'php_ini_timezone':
          path => '/etc/php.ini',
          match => '^date.timezone = .*',
          line => "date.timezone = \"${::timezone::timezone}\"",
          require => Class['Apache::Mod::Php'],
          before => Apache::Vhost::Custom["${web_ui_hostname}"]
        }
        file_line { 'php_ini_comment':
          path => '/etc/php.ini',
          after => '^\[PHP\]$',
          line => "; Managed by Puppet, please be careful about editing.",
          require => Class['Apache::Mod::Php'],
        }
      }
      
      # Open port 443 HTTPS and port 80 HTTP
      firewall {
        '100 allow HTTPS access':
          dport    => 443,
          ctstate  => 'NEW',
          proto    => tcp,
          action   => accept,
          chain    => 'INPUT';
        '100 allow HTTP access':
          dport    => 80,
          ctstate  => 'NEW',
          proto    => tcp,
          action   => accept,
          chain    => 'INPUT';
      }

      # Now install Icinga2 web UI, with additional non-critical params specified in hiera.
      # TODO: Modify icingaweb2 module to use PostgreSQL instead
      # TODO: More elegant way to initialize icingaweb2 database (e.g. conditional on first run)
      class { '::icingaweb2':
        install_method => 'package',
        initialize => true,
        manage_apache_vhost => false,
        ido_db => $ido_db,
        ido_db_host => $ido_db_host,
        ido_db_port => $ido_db_port,
        ido_db_user => $ido_db_user,
        ido_db_pass => $ido_db_pass,
        ido_db_name => $ido_db_name,
        web_db => 'mysql',
        web_db_host => $web_db_host,
        web_db_name => $web_db_name,
        web_db_user => $web_db_user,
        web_db_port => $web_db_port,
        web_db_pass => $web_db_pass,
      }
      Mysql::Db["${web_db_name}"] -> Class['Icingaweb2::Initialize']
      Class['Mysql::Server'] -> Class['Icingaweb2::Initialize']

      # RedHat OS's require EPEL for nagios plugins needed by icinga2
      Class['Profile::Pkg_repos'] -> Class['Icingaweb2::Install']

      # Redirect port 80 incoming to port 443
      apache::vhost { "${web_ui_hostname} SSL redirect":
        servername      => $web_ui_hostname,
        port            => '80',
        redirect_status => 'permanent',
        redirect_dest   => "https://${web_ui_hostname}/",
        manage_docroot => false,
        docroot => false,
      }
      # Icingaweb2 apache vhost support is incomplete, so do it ourselves
      apache::vhost::custom { $web_ui_hostname :
        content  => template('profile/monitoring/apache_icingaweb2_vhost.erb'),
      }
      Class['Apache::Mod::Php'] -> Apache::Vhost::Custom["$web_ui_hostname"]
      Class['Apache::Mod::Ssl'] -> Apache::Vhost::Custom["$web_ui_hostname"]
      Class['Icingaweb2'] -> Apache::Vhost::Custom["$web_ui_hostname"]

      # Make sure the default apache icingaweb2 config installed by the rpm is removed
      file { '/etc/httpd/conf.d/icingaweb2.conf' :
        ensure => absent,
      }
      
      # Install related components for Icinga2 web UI.
      include icingaweb2::mod::monitoring
      package { 'icingacli':
        ensure => installed,
        require => Class['Icingaweb2'],
        notify => Exec['icingaweb2_icingaadmin_update_password'],
      }
      
      # Set password for icingaadmin user; note need to escape '$' for MySQL
      $icingaadmin_pass_real = regsubst($icingaadmin_pass, '\$', '\\\$', 'G')
      exec { 'icingaweb2_icingaadmin_update_password':
        command => "mysql --defaults-file='/root/.my.cnf' ${web_db_name} -e \" UPDATE icingaweb_user SET password_hash='${icingaadmin_pass_real}' WHERE name='icingaadmin';\"",
        refreshonly => true,
      }

    }
    default: {
      fail ( "Class profile::monitoring::icingaweb2 not supported for ${::osfamily}")
    }  
  }
}
