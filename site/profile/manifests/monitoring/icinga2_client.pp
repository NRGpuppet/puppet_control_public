# == Class: profile::monitoring::icinga2_client
#
# This class is a wrapper around the icinga2 module, installing the Icinga v2 monitoring client.
# This class is expected to be included on all nodes to be monitered by the Icinga2 server.  This
# class is safe to include alongside profile::monitoring::icinga2_server, provided the
# 'master_endpoint_host' parameter below is correctly set to my $::fqdn, and/or the 'role'
# top-level fact is set to 'role::monitoring::icinga2_server'.
#
# More info about this class in the profile module README.md
# 
# === Parameters
#
# Parameters this class accepts.
#
# [*master_endpoint_host*]
#   Required, the FQDN of the Icinga2 server that the client should point to.  Specifically, this
#   is the host value put in the endpoint created for the Icinga2 server.  Note value this would be
#   identical to $::fqdn on the Icinga2 server itself, unless there are multiple servers.
#
# [*master_endpoint_port*]
#   The TCP port to connect to on the Icinga2 master.  Note this has to be a string per limitations
#   of Icinga2 module.  Default '5665'.
#
# [*master_zone*]
#   The name of the master zone, which will contain the endpoint pointing to the Icinga2 server.
#   Default 'master'.
#
# [*debuglog*]
#   Whether to enable the debuglog feature of Icinga2 client. Default false.
# 
# [*postgres_checks*]
#   Optional, hash of postgres checks.  More TDB.  If this is isn't undef, the check_postgres
#   plugin will be installed on this node.  Default undef.
#
# [*webinject_checks*]
#   Optional, hash of webinject checks.  More TDB.  If this is isn't undef, the check_webinject
#   plugin will be installed on this node.  Default undef.
#
# [*hosts*]
#   Optional, hash of hosts for create_resources('@@icinga2::object::host', $hosts), i.e.
#   which will be exported to the Icinga2 server.  This is useful if THIS node has connectivity
#   to a remote host you want the Icinga2 server to monitor (i.e. via ping, SSH), but where
#   the Icinga2 server itself does not have connectivity.  Any hosts defined here should not
#   collide with any other exported host definitions, that is, any other nodes running the
#   profile::monitoring::icinga2_client class.  Default undef.
#
# === Variables
#
# Custom variables this class uses.
#
# [*role*]
#   If 'role' is defined as a custom top-level fact, this class will not install the Icinga2
#   client if role = 'role::monitoring::icinga2_server'.  This allows this class to be safely
#   included alongside the class profile::monitoring::icinga2_server.
#
# [*profile::monitoring::icinga2_client::host_vars*]
#   The hiera parameter 'profile::monitoring::icinga2_client::host_vars' can be a hash of
#   key => value pairs to pass along for 'vars' on the exported Icinga2 host definition.  Note
#   deep merging is possible, since this isn't being passed to the class as a parameter but
#   rather looked up via hiera_hash().  The host_vars hash must have a 'remote_client' value
#   set to %{::fqdn} .
#
# [*profile::monitoring::icinga2_client::host_groups*]
#   The hiera parameter 'profile::monitoring::icinga2_client::host_groups' can be an array of
#   host groups to put in the exported Icinga2 host definition.  Like above, this value is
#   retrieved via hiera_array().  Note the Icinga2 server must have the host groups defined, i.e.
#   via the profile::monitoring::icinga2_server::hostgroups parameter.
#
# === Examples
#
#  class { '::profile::monitoring::icinga2_client':
#    master_endpoint_host => 'iginga.mydomain.com',
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::monitoring::icinga2_client (
  String $master_endpoint_host,
  String $master_zone = 'master',
  String $master_endpoint_port = '5665',
  Boolean $debuglog = false,
  Optional[Hash] $postgres_checks = undef,
  Optional[Hash] $webinject_checks = undef,
  Optional[Hash] $hosts = undef,
  ) {

  include ::profile::monitoring::client::icinga2_client_shared
  include ::profile::pkg_repos

  # Declare the appropriate Icinga2 class, based on OS
  case $::osfamily {
    /^(Debian|RedHat)$/ : {
      if ($::role != 'role::monitoring::icinga2_server') and ($master_endpoint_host != $::fqdn)  {

        # Install Icinga 2, with additional non-critical params specified in hiera.
        class { '::icinga2':
          manage_database => false,
          config_template => 'profile/monitoring/icinga2.conf.erb',
        }

        # RedHat OS's require EPEL for nagios plugins needed by icinga2
        Class['Profile::Pkg_repos'] -> Class['Icinga2']

        # Put our plugin dirs under Puppet control
        # TODO: Make less CentOS specific
        file_line {
          'icinga2_constants_comment':
            path => '/etc/icinga2/constants.conf',
            after => '^ \* the other configuration files.*$',
            line => ' * This file is managed by the profile Puppet module; please be careful editing.',
            require => Class['Icinga2'];
          'icinga2_constants_plugindir':
            path => '/etc/icinga2/constants.conf',
            match => '^const PluginDir =.*$',
            line => "const PluginDir = \"${::profile::monitoring::client::icinga2_client_shared::plugindir}\"",
            require => Class['Icinga2'];
          'icinga2_constants_plugincontribdir':
            path => '/etc/icinga2/constants.conf',
            match => '^const PluginContribDir =.*$',
            line => "const PluginContribDir = \"${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}\"",
            require => Class['Icinga2'];
          'icinga2_constants_manubulonplugindir':
            path => '/etc/icinga2/constants.conf',
            match => '^const ManubulonPluginDir =.*$',
            line => "const ManubulonPluginDir = \"${::profile::monitoring::client::icinga2_client_shared::manubulonplugindir}\"",
            require => Class['Icinga2'];
        }

        # Icinga2 install deposits a hosts.conf that conflicts with puppet
        # TODO: Revise to use $::icinga2::purge_confd = true, if needed.
        file { '/etc/icinga2/conf.d/hosts.conf':
          ensure => absent,
        }

        if $debuglog {
          icinga2::feature { 'debuglog':
            manage_file => false,
          }
        }

        # Open port 5665 HTTP for incoming Icinga2 master
        firewall { '100 allow Icinga2 master access':
          dport    => 5665,
          ctstate  => 'NEW',
          proto    => tcp,
          action   => accept,
          chain    => 'INPUT',
        }

        # Create Icinga2 API listener object locally, pointing to master zone
        include icinga2::pki::puppet
        class { '::icinga2::feature::api':
          accept_commands => true,
          parent_zone => $master_zone,
        }

        # Declare master zone and endpoint
        $endpoints_master = {
          $master_endpoint_host => {
            host => $master_endpoint_host,
            port => $master_endpoint_port,
          },
        }
        icinga2::object::zone { $master_zone:
          endpoints =>  $endpoints_master,
        }

      }
      
    }
    /^(Solaris|Illumos)$/ : {
      if ($::role != 'role::monitoring::icinga2_server') and ($master_endpoint_host != $::fqdn) {
        # Icinga2 client under OmniOS farmed out to nrgillumos module
        class { '::nrgillumos::icinga2_client':
          master_zone => $master_zone,
          master_endpoint_host => $master_endpoint_host,
          master_endpoint_port => $master_endpoint_port,
        }
      }
    }
    default: {
      fail ( "Class profile::monitoring::icinga2_client not supported for ${::osfamily}")
    }
  }

  # Now include classes to install any contrib plugins.  Note that plugin config is deployed
  # on Icinga2 server via profile::monitoring::icinga2_server class, and then executed locally
  # on this node via Icinga2 server->client API tunnel.
  if $postgres_checks {
    include ::profile::monitoring::client::icinga2_plugin_check_postgres
  }
  if $webinject_checks {
    include ::profile::monitoring::client::icinga2_plugin_check_webinject 
  }

  # Retrieve host vars from hiera (i.e. permitting deep merge), or set default if none found
  $host_vars = hiera_hash('profile::monitoring::icinga2_client::host_vars', undef)
  if (! $host_vars) {
    $host_vars_real = { 
      os => $::kernel,
      virtual => $::is_virtual,
      distro => $::operatingsystem,
      remote_client => "${::fqdn}"
    }
  }
  else {
    $host_vars_real = $host_vars
  }

  # Retrieve host groups from hiera (i.e. permitting deep merge), or set default if none found
  $host_groups = hiera_array('profile::monitoring::icinga2_client::host_groups', undef)
  if (! $host_groups) {
    # TODO: generalize for OS's besides Linux
    $host_groups_real = ['linux-servers']
  }
  else {
    $host_groups_real = $host_groups
  }

  # Now include resources every monitoring client exports to server
  include ::profile::monitoring::client::icinga2_client_exported

}
