# == Class: profile::monitoring::client::icinga2_plugin_check_mysql_health
#
# Private class to install check_mysql_health plugin for Icinga v2 monitoring client. Invoked
# as needed by profile::monitoring::icinga2_client.
#
# Refs:
# https://github.com/lausser/check_mysql_health
# https://labs.consol.de/nagios/check_mysql_health/
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::monitoring::client::icinga2_plugin_check_mysql_health {

  include ::profile::monitoring::client::icinga2_client_shared

  # Deploy check_postgres script, based on OS
  case $::osfamily {
    /^(Debian|RedHat)$/ : {

    # TODO: install the plugin!

    }
    default: {
      fail ( "Class profile::monitoring::client::icinga2_plugin_check_postgres not supported for ${::osfamily}")
    }
  }
}
