# == Class: profile::monitoring::client::icinga2_client_exported
#
# Private class to deploy exported resources necessary for Icinga v2 monitoring client. Invoked
# by profile::monitoring::icinga2_client.
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::monitoring::client::icinga2_client_exported {

  include ::profile::monitoring::client::icinga2_client_shared
  include ::icinga2

  # Create endpoints for exported Zone that master will collect
  $endpoints = {
    "${::fqdn}" => {
      host => $::fqdn,
    },
  }

  # Declare the appropriate Icinga2 objects, based on OS
  case $::osfamily {
    /^(Debian|RedHat|Solaris|Illumos)$/ : {

      # Export icinga2 zone defintion for myself, but only if I'm not the Icinga2 master
      if (($::role != 'role::monitoring::icinga2_server') and
      ($::profile::monitoring::icinga2_client::master_endpoint_host != $::fqdn)) {
        @@icinga2::object::zone { "${::fqdn}_remote":
          endpoints =>  $endpoints,
          tag => [ "icinga2" ],
        }
      }

      # Export icinga2 host object declaration for myself.
      @@icinga2::object::host { $::fqdn:
        display_name => $::fqdn,
        ipv4_address => $::networking::ip,
        ipv6_address => $::networking::ip6,
        groups => $::profile::monitoring::icinga2_client::host_groups_real,
        vars => $::profile::monitoring::icinga2_client::host_vars_real,
        tag => [ "icinga2" ],
      }

      # Export any 3rd party host declarations provided via params
      $export_defaults = {
        tag => [ "icinga2" ],
      }
      if $::profile::monitoring::icinga2_client::hosts {
        create_resources('@@icinga2::object::host', $::profile::monitoring::icinga2_client::hosts, $export_defaults)
      }

      # Export apply_service resources for any plugin checks
      if $::profile::monitoring::icinga2_client::postgres_checks {
        $::profile::monitoring::icinga2_client::postgres_checks.each |$c, $check| {
          @@icinga2::object::apply_service { $c:
            check_command => 'postgres',
            display_name => $check[display_name],
            command_endpoint => 'host.vars.remote_client',
            assign_where => "host.display_name == \"${::fqdn}\"",
            check_interval => $check[check_interval],
            custom_append => [ "vars.postgres_host = \"${check[host]}\"",
                               "vars.postgres_dbname = \"${check[dbname]}\"",
                               "vars.postgres_dbuser = \"${check[dbuser]}\"",
                               "vars.postgres_dbpass = \"${check[dbpass]}\"",
                               "vars.postgres_action = \"${check[action]}\"" ],
            tag => [ "icinga2" ],
          }
        }
      }
      if $::profile::monitoring::icinga2_client::webinject_checks {
        $::profile::monitoring::icinga2_client::webinject_checks.each |$c, $check| {
          # Deploy webinject XML files locally
          file {
            "${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}/webinject/${c}_config.xml":
              owner => $::icinga2::config_owner,
              group => $::icinga2::config_group,
              mode => $::icinga2::config_mode,
              content => $check[config_content],
              require => Class['Profile::Monitoring::Client::Icinga2_plugin_check_webinject'];
            "${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}/webinject/${c}_testcases.xml":
              owner => $::icinga2::config_owner,
              group => $::icinga2::config_group,
              mode => $::icinga2::config_mode,
              content => $check[testcases_content],
              require => Class['Profile::Monitoring::Client::Icinga2_plugin_check_webinject'];
          }

          # Now export the apply_service resource
          @@icinga2::object::apply_service { $c:
            check_command => 'webinject',
            display_name => $check[display_name],
            command_endpoint => 'host.vars.remote_client',
            assign_where => "host.display_name == \"${::fqdn}\"",
            check_interval => $check[check_interval],
            custom_append => [ "vars.webinject_config_file = \"${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}/webinject/${c}_config.xml\"",
                               "vars.webinject_testcase_file = \"${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}/webinject/${c}_testcases.xml\"",
                               'vars.webinject_report_type = "nagios"',
                               "vars.webinject_output = \"/tmp/icinga2_webinject_${c}_\"" ],
            tag => [ "icinga2" ],
          }          
        }
      }
    }
  }
}
