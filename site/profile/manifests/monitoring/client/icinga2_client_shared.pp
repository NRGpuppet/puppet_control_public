# == Class: profile::monitoring::client::icinga2_client_shared
#
# Private class to contain shared varibles, etc, for use by Icinga v2 monitoring server and
# client.  Stored with client classes, since Icinga2 server also is a client.
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::monitoring::client::icinga2_client_shared {

  # TODO: Make less CentOS specific
  $plugindir = '/usr/lib64/nagios/plugins'
  $plugincontribdir = '/usr/lib64/nagios/plugins'
  $manubulonplugindir = '/usr/lib64/nagios/plugins'
  $check_postgres_repo = 'https://github.com/bucardo/check_postgres'

}
