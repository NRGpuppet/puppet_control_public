# == Class: profile::monitoring::client::icinga2_plugin_check_webinject
#
# Private class to install check_webinject plugin for Icinga v2 monitoring client. Invoked
# as needed by profile::monitoring::icinga2_client.
#
# Refs:
# http://jon.netdork.net/2011/03/19/using-nagios-to-monitor-webpages/
# http://search.cpan.org/~nierlein/Webinject-1.90/lib/Webinject.pm
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::monitoring::client::icinga2_plugin_check_webinject {

  include ::profile::monitoring::client::icinga2_client_shared

  # Deploy check_webinject script, based on OS
  case $::osfamily {
    /^(Debian|RedHat)$/ : {

      # Handle prereqs
      include ::cpan

      cpan { "Fatal":
        ensure  => present,
        require => Class['::cpan'],
      }
      ->
      cpan { "XML::SAX":
        ensure  => present,
      }
      ->
      cpan { "XML::SAX::Expat":
        ensure  => present,
      }
      ->
      cpan { "XML::Simple":
        ensure  => present,
      }
      ->
      cpan { "ExtUtils::CBuilder":
        ensure  => present,
      }
      ->
      cpan { "Path::Class":
        ensure  => present,
      }
      ->
      cpan { "Crypt::SSLeay":
        ensure  => present,
      }
      ->
      cpan { "Webinject":
        ensure  => present,
      }

      # Deploy check_webinject to the plugins dir and create dir for XML files
      file {
        "${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}/check_webinject":
          ensure => link,
          target => '/usr/local/bin/webinject.pl',
          require => Cpan['Webinject'];
      }
      ->
      file {
        "${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}/webinject":
          ensure => directory,
      }
    }
    default: {
      fail ( "Class profile::monitoring::client::icinga2_plugin_check_webinject not supported for ${::osfamily}")
    }
  }
}
