# == Class: profile::monitoring::client::icinga2_plugin_check_postgres
#
# Private class to install check_postgres plugin for Icinga v2 monitoring client. Invoked
# as needed by profile::monitoring::icinga2_client.
#
# Note this class won't try to verify database connectivity.  We want the plugin to be able
# to detect lacking connectivity.
#
# Ref: https://bucardo.org/wiki/Check_postgres
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::monitoring::client::icinga2_plugin_check_postgres {

  include ::profile::monitoring::client::icinga2_client_shared

  # Deploy check_postgres script, based on OS
  case $::osfamily {
    /^(Debian|RedHat)$/ : {

      # Handle prereqs
      include ::git
      include ::cpan

      case $::osfamily {
        "Debian": {
          package { 'data-dumper':
            ensure => present,
            before => File['/opt/puppet_check_postgres_repo'],
          }
        }
        "RedHat": {
          case $::operatingsystemmajrelease {
            "5", "6": {
              cpan { "Data::Dumper":
                ensure  => present,
                require => Class['::cpan'],
                before => File['/opt/puppet_check_postgres_repo'],
              }
            }
            "7": {
              package { 'perl-Data-Dumper':
                ensure => present,
                before => File['/opt/puppet_check_postgres_repo'],
              }
            }
          }
        }
      }

      # Check out check_postgres repo
      file { '/opt/puppet_check_postgres_repo':
        ensure => directory,
      }
      ->

      vcsrepo { '/opt/puppet_check_postgres_repo':
        ensure => present,
        provider => git,
        source => $::profile::monitoring::client::icinga2_client_shared::check_postgres_repo,
        require => Class['Git'],
        notify => Exec['icinga2_check_postgres_symlinks'];
      }

      # Deploy check_postgres.pl to the plugins dir
      file { 
        "${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}/postgres":
          ensure => directory,
          require => Class['Icinga2'],
          notify => Exec['icinga2_check_postgres_symlinks'];
        "${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}/check_postgres.pl":
          ensure => link,
          target => '/opt/puppet_check_postgres_repo/check_postgres.pl',
          require => [Class['Icinga2'], Vcsrepo['/opt/puppet_check_postgres_repo']];
      }

      # Create (or refresh?) symlinks
      exec { 'icinga2_check_postgres_symlinks':
        command => 'perl ../check_postgres.pl --symlinks',
        refreshonly => true,
        cwd => "${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}/postgres",
        require => [ File["${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}/check_postgres.pl"],
                     File["${::profile::monitoring::client::icinga2_client_shared::plugincontribdir}/postgres"] ];
      }
    }
    default: {
      fail ( "Class profile::monitoring::client::icinga2_plugin_check_postgres not supported for ${::osfamily}")
    }
  }
}
