# == Class: profile::base
#
# This manifest is the main profile class for this environment of the NRG puppet
# control_repo.  This class is intended to run first, before any other classes.
#
# In general, this profile class replaces the global manifests like site.pp and
# templates.pp previously used in puppet v2 deployment, although an environment-wide
# site.pp does remain in use in lesser form in this control_repo.
#
# Just put high-level glue code here.  Note that shared modules/classes are preferably
# included via hiera, e.g. in common.yaml.  Likewise, parameters should also be in hiera.
#
# Also, note that elaborate configuration which is highly OS-specific is best handled by
# custom modules, even if reasonably independent of NRG's applications.
#
# This classes can only be versioned at the environment level in the puppet_control git
# repo.  More fine-grained versioning is best done at the module level in Puppetfile,
# hence recommendation above to use custom modules.
#
# More info about this class in the profile module README.md
#
# === Parameters
#
# Parameters used by this class.
#
# [*files*]
#   Hash of file resources to pass to create_resources('file', $files)
#
# [*services*]
#   Hash of service resources to pass to create_resources('service', $services)
#
# [*packages*]
#   Hash of package resources to pass to create_resources('package', $packages)
#
# [*execs*]
#   Hash of exec resources to pass to create_resources('exec', $execs)
#
# === Variables
#
# Custom variables used by this class.
#
# NONE
#
# === Examples
#
#  class { '::profile::base':
#    packages => { 'openssl-devel' => { 'ensure => 'installed' },
#                  'bluez-utils' => { 'ensure => 'absent' }}
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::base (
  Optional[Hash] $files = undef,
  Optional[Hash] $services = undef,
  Optional[Hash] $packages = undef,
  Optional[Hash] $execs = undef )
{

  # Redhat OS's need EPEL repo
  if $::osfamily == 'RedHat' {
    include ::epel
    $pkgs_defaults = { require => Class['Epel'] }
  }
  else {
    $pkgs_defaults = undef
  }

  # Declare resources specified in hiera or params
  if $files {
    create_resources('file', $files)
  }
  if $services {
    create_resources('service', $services)
  }
  if $packages {
    create_resources('package', $packages, $pkgs_defaults)
  }
  if $execs {
    create_resources('exec', $execs)
  }
  
  # RHEL/CentOS 7 hosts use chrony module, others use ntp.
  if $::operatingsystemmajrelease == "7" and $::osfamily == "RedHat"  {
    class { '::chrony': }
  }
  else {
    class { '::ntp': }
    # Make sure NTP drift file exists
    file { "${::ntp::driftfile}":
      ensure => present,
    }
  }

  # Include firewall module, if supported
  case $::osfamily {
    /^(Debian|RedHat)$/ : {
      # Include firewall
      include firewall

      # Now open incoming port 22
      firewall { '010 allow SSH access':
        dport    => 22,
        ctstate  => 'NEW',
        proto    => tcp,
        action   => accept,
        chain    => 'INPUT',
      }
    }
    /^(Solaris|Illumos)$/ : {
      # TODO: open port 22 under Illumos
    }
    default: {
      # Here for completeness
    }
  }

}
