# == Class: profile::puppet::hiera
#
# This class deploys hiera configuration for puppet 4.  This class presently assumes CentOS 7.
# This profile::puppet::hiera class is included by the profile::puppet::puppetserver class.
#
# For bootstrapping, this class can be applied in puppet stand-alone mode first, i.e. to ensure
# hiera-eyaml config is in place, before applying the puppetserver role class
# role::puppet::puppetserver_int_db or role::puppet::puppetserver_ext_db.
#
# More info about this class in the profile module README.md
# 
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.  Note
# that those parameters which lookup a value in hiera and which don't have any default
# specfied below will throw an error if hiera lookup fails.  This behavior is
# intentional.
#
# [*sample_variable*]
#   Explanation of how this variable affects the function of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { '::profile::puppet::hiera':
#    hierarchy => [ 'fqdn/%{::fqdn}',
#                   'role/%{::role}',
#                   'virtual/%{::virtual}',
#                   'operatingsystem/%{::operatingsystem}%{::operatingsystemmajrelease}'
#                   'secure',
#                   'common' ]
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::puppet::hiera (
  $hierarchy = undef,
  $eyaml = true,
  $create_keys = true )
{  
  include profile::puppet::shared

  $hiera_eyaml_priv_key = "${::settings::codedir}/keys/private_key.pkcs7.pem"
  $hiera_eyaml_pub_key = "${::settings::codedir}/keys/public_key.pkcs7.pem"
  $puppet_public_bindir = '/opt/puppetlabs/bin'
  $puppet_jruby_bindir = '/opt/puppetlabs/server/data/puppetserver/jruby-gems/bin'

  # Check if we're bootstrapping
  if (defined(Class['Puppet'])) {
    require puppet
  }
  else {
    # Create dummy puppet user/group to satifsy hiera module during bootstrap
    user { 'puppet':
      gid => 'puppet',
      shell => '/sbin/nologin',
      home => '/dev/null',
      ensure => present,
      require => Group['puppet'];
    }
    group { 'puppet':
      ensure => present;
    }
    # If EYAML keys already exist during bootstrap, set correct perms
    if (file_exists($hiera_eyaml_priv_key) == 1) and (! $create_keys) {
      file { $hiera_eyaml_priv_key:
        ensure => file,
        owner => 'puppet',
        group => 'puppet',
        mode => '0600',
        require => [ User['puppet'], Group['puppet'] ];
      }
      file { $hiera_eyaml_pub_key:
        ensure => file,
        owner => 'puppet',
        group => 'puppet',
        mode => '0644',
        require => [ User['puppet'], Group['puppet'] ];
      }
    }
  }

  # Per hiera convention, the first definition of any value takes priority.  Also,
  # the convention is to rank hierarchy levels from most specific (e.g. node FQDN)
  # to least specific (node OS), with a common level last. In this instance, tho,
  # the secure hierarchy level is high in the hierarchy to ensure any values specified
  # there take priority.

  # Specify an initial hierarchy for hiera data, if we are bootstrapping and none
  # is specified otherwise, i.e. via my own parameter or in hiera.
  if (! $hierarchy) and (hiera("hiera::hierarchy", undef) == undef) {
    $hierarchy_real = [ 'secure',
                        'fqdn/%{::fqdn}',
                        'role/%{::role}',
                        'virtual/%{::virtual}',
                        'operatingsystem/%{::operatingsystem}%{::operatingsystemmajrelease}',
                        'operatingsystem/%{::operatingsystem}',
                        'common',
                        ]
  }
  # Otherwise, my own parameter overrides any hierarchy defined elsewhere
  elsif $hierarchy {
    $hierarchy_real = $hierarchy
  }
  # Otherwise, let hiera module use hierarchy defined elsewhere (i.e. in hiera)
  else {
    $hierarchy_real = hiera("hiera::hierarchy", undef)
  }

  # If hiera-eyaml enabled, throw up a notice if hiera-eyaml keys not found and $create_keys = 
  # true, i.e. to alert that such keys will be auto-generated.
  if $eyaml and (file_exists($hiera_eyaml_priv_key) == 0) and $create_keys {
    notify { "Hiera on ${::fqdn} creating new EYAML key ${hiera_eyaml_priv_key}." :}
  }
  # Otherwise, throw error if hiera-eyaml keys not found and $create_keys = false.
  elsif $eyaml and (file_exists($hiera_eyaml_priv_key) == 0) and (! $create_keys) {
    fail("No EYAML key ${hiera_eyaml_priv_key} found on ${::fqdn} and create_keys not enabled.")
  }

  # Declare the hiera module class.  Additional params can be passed to module via
  # via hiera data (i.e. after bootstrapping).
  class { 'hiera':
    hierarchy => $hierarchy_real,
    create_keys => $create_keys,
    datadir => "${::settings::environmentpath}/${::environment}/hiera",
    eyaml => $eyaml,
    eyaml_extension => 'eyaml',
    merge_behavior => 'deeper',
  }

  if (defined(Class['Puppet'])) {
    # Install hiera-eyaml gem into puppetserver's jruby stacks, when available.  Note
    # this assumes JVM-based puppetserver.
    exec { 'install hiera-eyaml gem into puppetserver jruby':
      command => "${puppet_public_bindir}/puppetserver gem install hiera-eyaml --no-ri --no-rdoc",
      creates => "${puppet_jruby_bindir}/eyaml",
      require => Package['puppetserver'];
    }
  }
}
