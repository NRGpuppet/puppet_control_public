# == Class: profile::puppet::puppetserver_dev
#
# This class deploys puppetserver v2.x (aka puppet 4) in emphemeral development mode. Note
# this deployment is assumed to be expendable, i.e. NOT for production use.  This class
# presently assumes CentOS 7.
#
# This profile::puppet::puppetserver_dev class should NOT be run on the same node as
# profile::puppet::puppetca to avoid conflict.
#
# If the hiera tool is desired on this puppetserver, then the class profile::puppet::hiera
# needs to be run on this node, after this class.
#
# If the r10k tool is desired on this puppetserver, then the class profile::puppet::r10k
# needs to be run on this node, after this class.
#
# This class will also deploy a puppetDB on the same machine via the
# profile::puppet::puppetdb class.
#
# More info about this class in the profile module README.md
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.  Note
# that those parameters which lookup a value in hiera and which don't have any default
# specfied below will throw an error if hiera lookup fails.  This behavior is
# intentional.
#
# [*sample_variable*]
#   Explanation of how this variable affects the function of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { 'profile::puppet::puppetserver':
#    blahblah => 'blah',
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::puppet::puppetserver_dev (
  $bb_username = undef,
  $bb_password = undef,
  $delayed_puppet_runmode = undef,
  $server_autosign_nodes = undef,
  $delayed_server_reports = 'store',
  $delayed_server_storeconfigs_backend = undef,
) {
  
  include profile::puppet::shared
  include firewall
  require java
  include profile::hosts
  
  # Deploy internal puppetDB
  include profile::puppet::puppetdb
  Class['Puppetdb::Server'] -> Class['Puppet']
  Host["${::fqdn}"] -> Class['Puppetdb::Server']

  # If puppetserver class hasn't yet run to completion, don't enable puppet-agent to avoid possible
  # concurrency issues intefering w/ bootstrap.  Using file_exists() seems necessary to ensure
  # puppet-agent gets its proper "runmode" parameter after bootstrap.
  $delayed_puppet_runmode_real = file_exists("${::settings::hostpubkey}") ? {
    1 => $delayed_puppet_runmode,
    default => 'none'
  }

  # Open port 8140 for incoming puppet agent connections
  firewall { '100 allow puppet agent access':
    dport    => 8140,
    ctstate  => 'NEW',
    proto    => tcp,
    action   => accept,
    chain    => 'INPUT',
  }

  # Install JVM-based puppetserver, configured for directory environments.
  # This will implicitly include puppetdb::master::config class if db host specified.
  # Additional params can be passed to puppet module via hiera.
  class { '::puppet': 
    server => true,
    runmode => $delayed_puppet_runmode_real,
    server_implementation => 'puppetserver',
    server_external_nodes => '',
    server_environments   => [],
    server_puppetdb_swf => true,
    server_puppetdb_host => 'localhost',
    server_storeconfigs_backend => $delayed_server_storeconfigs_backend,
    server_reports => $delayed_server_reports,
  }
  contain '::puppet'

  # Various puppetserver paths
  $puppet_public_bindir = $puppet::puppetca_path
  $puppet_confdir = $puppet::dir
  $puppet_hostcert = $::settings::hostcert
  $puppet_hostprivkey = $::settings::hostprivkey
  $puppet_localcacert = $::settings::localcacert
  $puppetserver_dir = '/etc/puppetlabs/puppetserver'

  # TODO: Establish the /etc/puppetmaster/... path from remote NFS mount

  # Add entry to auth.conf to permit local access to Puppetserver API for environment refresh
  puppet_authorization::rule { 'environment-cache':
    match_request_path   => '/puppet-admin-api/v1/environment-cache',
    match_request_type   => 'path',
    match_request_method => 'delete',
    allow                => $::fqdn,
    sort_order           => 500,
    path                 => "${puppetserver_dir}/conf.d/auth.conf",
  }
  Puppet_authorization::Rule['environment-cache'] ~> Class['Puppet']

  # Deploy environment refresh scipt
  $puppet_environment = $::environment
  file { 'refresh_puppet_environment_script':
    path => '/usr/local/bin/refresh_puppet_environment.sh',
    ensure => present,
    mode => '0755',
    content => template('profile/puppet/refresh_puppet_environment.sh.erb'),
    require => [ Class['Puppet'], Puppet_authorization::Rule['environment-cache'] ],
  }

  # Grant root user read permission to bitbucket mercurial repos
  # TODO: sort out redundancies b/w this mercurial .hgrc access file and root user RSA key
  if $bb_username {
    $bb_prefix = "https://bitbucket.org/${bb_username}"
    $root_hgrc = '/root/.hgrc'
    concat { $root_hgrc:
      ensure => present,
      owner => 'root',
      group => 'root',
      mode  => '0600',
      before => Class['Puppet'];
    }
    concat::fragment { 'root_hgrc_bitbucket':
      target => $root_hgrc,
      content => "\n[auth]\nbb.prefix = ${bb_prefix}\nbb.username = ${bb_username}\nbb.password = ${bb_password}\n";
    }
  }
  
  # Populate autosign.conf
  if $server_autosign_nodes {
    $server_autosign_nodes_real = join($server_autosign_nodes, "\n")
    file { "${puppet_confdir}/autosign.conf":
      ensure => present,
      content => $server_autosign_nodes_real,
      notify => Service['puppetserver'],
    }
  }

  # TODO: resolve race condition that requires these resources for local puppetdb be
  # declared after ::puppet class above, with the convoluted parameters below.
  if defined(Class['Puppetdb::Server']) {
    # Deploy SSL certs for embedded PuppetDB
   exec { 'puppetdb_ssl_setup':
      creates => $::puppetdb::ssl_cert_path,
      command => "${puppet_public_bindir}/puppetdb ssl-setup",
      require => [ Package[$::puppetdb::puppetdb_package], Class['Puppet::Server::Config'] ],
      notify => Service['puppetdb'],
    }
    $puppetserver_complete_require = [ Class['Puppet::Server'], Exec['puppetdb_ssl_setup'] ]
  }
  else {
    $puppetserver_complete_require = Class['Puppet::Server']
  }

  # Put exported file in shared_dir to signal puppetserver bootstrap completion
  @@file { 'puppetserver_dev_complete':
    ensure => present,
    path => "${profile::puppet::shared::shared_dir}/puppetserver_dev_complete",
    content => 'This is a dummy file used to signal Puppetserver_dev class execution completed.  Do not edit.',
    require => $puppetserver_complete_require,
  }

  # Updates to PuppetDB config w/ external PuppetDB should trigger Puppetserver refresh
  # TODO (Maybe): Update for use with Apache passenger puppetmaster
  if defined(Class['puppetdb::master::config']) {
    Class['puppetdb::master::puppetdb_conf'] ~> Service['puppetserver']
  }
}
