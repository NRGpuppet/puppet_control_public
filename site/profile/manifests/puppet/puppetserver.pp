# == Class: profile::puppet::puppetserver
#
# This class deploys puppetserver v2.x (aka puppet 4).  This class presently assumes CentOS 7.
# This profile::puppet::puppetserver class should NOT be run on the same node as
# profile::puppet::puppetca to avoid conflict.
#
# If the hiera tool is desired on this puppetserver, then the class profile::puppet::hiera needs
# to be run on this node, after this class.
#
# If the r10k tool is desired on this puppetserver, then the class profile::puppet::r10k needs
# to be run on this node, after this class.
#
# If another node running the profile::puppet::puppetdb class comes online, and this class is
# pointed to that node via the "delayed_server_puppetdb_host" parameter, this puppetserver
# will use that node as its puppetDB host.  If that node running the profile::puppet::puppetdb
# class is not (yet) available, then this class will revert to running the embedded puppetDB
# service locally until the external puppetDB host becomes available.
#
# Alternately, this class can be included with profile::puppet::puppetdb on the same node,
# to house the puppetserver and the non-embedded puppetDB on the same machine.
#
# Is it not recommended to deploy the embedded puppetDB as a permanent arrangement, since the
# embedded DB is deprecated and doesn't support robust backup measures.  You can have this class
# deploy the embedded puppetDB for testing purposes (e.g. Vagrant machine) by specifying a bogus
# hostname in the "delayed_server_puppetdb_host" parameter.
#
# Finally, when using this class to bootstrap a puppetserver, note this class may need to be
# applied up to *three* times to ensure puppet-agent gets its proper "runmode" parameter after
# completion of bootstrap.
#
# More info about this class in the profile module README.md
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.  Note
# that those parameters which lookup a value in hiera and which don't have any default
# specfied below will throw an error if hiera lookup fails.  This behavior is
# intentional.
#
# [*sample_variable*]
#   Explanation of how this variable affects the function of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { 'profile::puppet::puppetserver':
#    blahblah => 'blah',
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::puppet::puppetserver (
  $bb_username = undef,
  $bb_password = undef,
  $delayed_puppet_runmode = undef,
  $server_autosign_nodes = undef,
  $delayed_server_reports = 'store',
  $delayed_server_storeconfigs_backend = undef,
  $delayed_server_puppetdb_host = undef,
  ) {

  include profile::puppet::shared
  include firewall
  require java
  include profile::hosts

  # Collect exported resource from external puppetDB, if any, and notify puppet class to refresh.
  # Note this will still require puppet-agent run *twice* after exported resource available before
  # puppetserver applies the update config and restarts itself.  (TODO: Figure out how to get this
  # done sooner.)
  File <<| tag == "puppetdb_exported_resource_${delayed_server_puppetdb_host}" |>> {
    before => Class['Puppet'],
    notify => Class['Puppet'],
    require => undef,
  }

  # These calisthenics seem to be neccessary since puppet appears quite squirrelly about
  # testing exported resources.  I.e. "if defined(File[...])" never tests true on exported
  # file resource.
  $puppetdb_host_avail = file_exists("${profile::puppet::shared::shared_dir}/puppetdb_exported_resource_${delayed_server_puppetdb_host}") ? {
    1 => true,
    default => false,
  }

  # If external puppetDB host specified but unavailable, fallback to embedded puppetDB to
  # permit bootstrap completion
  if ( $delayed_server_puppetdb_host) and
  ($delayed_server_puppetdb_host != $::fqdn) and
  ($delayed_server_puppetdb_host != $::settings::certname) and
  (! ($delayed_server_puppetdb_host in $::settings::dns_alt_names)) and
  ($delayed_server_puppetdb_host != 'localhost') and
  ($delayed_server_puppetdb_host != '127.0.0.1') and
  (! $puppetdb_host_avail)
  {
    notify { "Puppetserver ${::fqdn} falling back to local embedded puppetDB since ${delayed_server_puppetdb_host} unavailable" :}

    $delayed_server_reports_real = 'store,puppetdb'
    $delayed_server_storeconfigs_backend_real = 'puppetdb'
    $delayed_server_puppetdb_host_real = $::fqdn

    # Include embedded puppetDB to let Puppetserver bootstrap complete
    class { '::puppetdb':
      database => 'embedded',
      ssl_set_cert_paths => true,
      ssl_listen_address => '0.0.0.0',
      manage_firewall => false,
    }
  }
  # Otherwise, if puppetDB host is me, add class dependency
  elsif ( $delayed_server_puppetdb_host) and
  (($delayed_server_puppetdb_host == $::fqdn) or
  ($delayed_server_puppetdb_host == $::settings::certname) or
  ($delayed_server_puppetdb_host in $::settings::dns_alt_names) or
  ($delayed_server_puppetdb_host == 'localhost') or
  ($delayed_server_puppetdb_host == '127.0.0.1'))
  {
    include profile::puppet::puppetdb
    Class['Puppetdb::Server'] -> Class['Puppet']

    $delayed_server_reports_real = $delayed_server_reports
    $delayed_server_storeconfigs_backend_real = $delayed_server_storeconfigs_backend
    $delayed_server_puppetdb_host_real = $delayed_server_puppetdb_host

    # Connection attempts to FQDN port 8081 will time out until FQDN is mapped to this machine's
    # internal AWS IP address.  Hosts module does this by default.
    if ($delayed_server_puppetdb_host == $::fqdn) {
      Host["${::fqdn}"] -> Class['Puppetdb::Server']
    }

  }
  # Otherwise, just let parameters pass thru unmolested
  else {
    $delayed_server_reports_real = $delayed_server_reports
    $delayed_server_storeconfigs_backend_real = $delayed_server_storeconfigs_backend
    $delayed_server_puppetdb_host_real = $delayed_server_puppetdb_host

    # TODO: Disable local embeded puppetDB instance once successful transition to external
    # puppetDB host completed?
  }

  # If puppetserver class hasn't yet run to completion, don't enable puppet-agent to avoid possible
  # concurrency issues intefering w/ bootstrap.  Using file_exists() seems necessary to ensure
  # puppet-agent gets its proper "runmode" parameter after bootstrap.
  $delayed_puppet_runmode_real = file_exists("${::settings::hostpubkey}") ? {
    1 => $delayed_puppet_runmode,
    default => 'none'
  }

  # Open port 8140 for incoming puppet agent connections
  firewall { '100 allow puppet agent access':
    dport    => 8140,
    ctstate  => 'NEW',
    proto    => tcp,
    action   => accept,
    chain    => 'INPUT',
  }

  # Install JVM-based puppetserver, configured for directory environments.
  # This will implicitly include puppetdb::master::config class if db host specified.
  # Additional params can be passed to puppet module via hiera.
  class { '::puppet': 
    server => true,
    runmode => $delayed_puppet_runmode_real,
    server_implementation => 'puppetserver',
    server_external_nodes => '',
    server_environments   => [],
    server_puppetdb_swf => true,
    server_puppetdb_host => $delayed_server_puppetdb_host_real,
    server_storeconfigs_backend => $delayed_server_storeconfigs_backend_real,
    server_reports => $delayed_server_reports_real,
  }
  contain '::puppet'

  # Various puppetserver paths
  $puppet_public_bindir = $puppet::puppetca_path
  $puppet_confdir = $puppet::dir
  $puppet_hostcert = $::settings::hostcert
  $puppet_hostprivkey = $::settings::hostprivkey
  $puppet_localcacert = $::settings::localcacert
  $puppetserver_dir = '/etc/puppetlabs/puppetserver'

  # TODO: Establish the /etc/puppetmaster/... path from remote NFS mount

  # Add entry to auth.conf to permit local access to Puppetserver API for environment refresh
  puppet_authorization::rule { 'environment-cache':
    match_request_path   => '/puppet-admin-api/v1/environment-cache',
    match_request_type   => 'path',
    match_request_method => 'delete',
    allow                => $::fqdn,
    sort_order           => 500,
    path                 => "${puppetserver_dir}/conf.d/auth.conf",
  }
  Puppet_authorization::Rule['environment-cache'] ~> Class['Puppet']

  # Deploy environment refresh scipt
  $puppet_environment = $::environment
  file { 'refresh_puppet_environment_script':
    path => '/usr/local/bin/refresh_puppet_environment.sh',
    ensure => present,
    mode => '0755',
    content => template('profile/puppet/refresh_puppet_environment.sh.erb'),
    require => [ Class['Puppet'], Puppet_authorization::Rule['environment-cache'] ],
  }

  # Grant root user read permission to bitbucket mercurial repos
  # TODO: sort out redundancies b/w this mercurial .hgrc access file and root user RSA key
  if $bb_username {
    $bb_prefix = "https://bitbucket.org/${bb_username}"
    $root_hgrc = '/root/.hgrc'
    concat { $root_hgrc:
      ensure => present,
      owner => 'root',
      group => 'root',
      mode  => '0600',
      before => Class['Puppet'];
    }
    concat::fragment { 'root_hgrc_bitbucket':
      target => $root_hgrc,
      content => "\n[auth]\nbb.prefix = ${bb_prefix}\nbb.username = ${bb_username}\nbb.password = ${bb_password}\n";
    }
  }
  
  # Populate autosign.conf
  if $server_autosign_nodes {
    $server_autosign_nodes_real = join($server_autosign_nodes, "\n")
    file { "${puppet_confdir}/autosign.conf":
      ensure => present,
      content => $server_autosign_nodes_real,
      notify => Service['puppetserver'],
    }
  }

  # TODO: resolve race condition that requires these resources for local puppetdb be
  # declared after ::puppet class above, with the convoluted parameters below.
  if defined(Class['Puppetdb::Server']) {
    # Deploy SSL certs for embedded PuppetDB
   exec { 'puppetdb_ssl_setup':
      creates => $::puppetdb::ssl_cert_path,
      command => "${puppet_public_bindir}/puppetdb ssl-setup",
      require => [ Package[$::puppetdb::puppetdb_package], Class['Puppet::Server::Config'] ],
      notify => Service['puppetdb'],
    }
    $puppetserver_complete_require = [ Class['Puppet::Server'], Exec['puppetdb_ssl_setup'] ]
  }
  else {
    $puppetserver_complete_require = Class['Puppet::Server']
  }

  # Put exported file in shared_dir to signal puppetserver bootstrap completion
  @@file { 'puppetserver_complete':
    ensure => present,
    path => "${profile::puppet::shared::shared_dir}/puppetserver_complete",
    content => 'This is a dummy file used to signal Puppetserver class execution completed.  Do not edit.',
    require => $puppetserver_complete_require,
  }

  # Updates to PuppetDB config w/ external PuppetDB should trigger Puppetserver refresh
  # TODO (Maybe): Update for use with Apache passenger puppetmaster
  if defined(Class['puppetdb::master::config']) and $puppetdb_host_avail {
    Class['puppetdb::master::puppetdb_conf'] ~> Service['puppetserver']
  }
}
