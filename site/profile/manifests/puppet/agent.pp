# == Class: profile::puppet::agent
#
# This class manages puppet-agent v1.x (aka puppet 4).  It is assumed the
# puppet agent has already been installed prior to running this manifest, so this
# manifest will only perform housekeeping chores, i.e. instantiating relevant
# puppet module(s) to ensure ongoing configuration management is in place.
#
# This class is intended to be run last or near last in the main stage.
#
# If the profile::puppet::agent class is run on the same node as
# profile::puppet::puppetca or profile::puppet::puppetserver, it will do nothing
# to avoid conflict.
#
# More info about this class in the profile module README.md
#
# === Parameters
#
# Parameters used by this class.
#
# NONE
#
# === Variables
#
# Custom variables this class uses.
#
# [*role*]
#   If 'role' is defined as a custom top-level fact, this class will not install the Puppet
#   agent class if role = 'role::puppet::puppetserver.*' or 'role::puppet::puppetca'.  This 
#   allows this class to be safely included alongside the classes 
#   profile::puppet::puppetserver_int_db, profile::puppet::puppetca, etc.
#
# === Examples
#
#  class { 'profile::puppet::agent':
#    blahblah => 'blah',
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::puppet::agent {

  include profile::puppet::shared

  # Don't install puppet-agent if we're a puppetserver or CA
  $role_real = $::role ? {
    undef => '',
    default => $::role,
  }
  if (! defined(Class['Profile::Puppet::Puppetserver'])) and (! defined(Class['Profile::Puppet::Puppetca'])) and ($role_real !~ '^role::puppet::puppetserver.*') and ($role_real != 'role::puppet::puppetca') {

    # Instantiate puppet agent module relevant to OS
    case $::osfamily {
      /^(Debian|RedHat)$/ : {
        # Include puppet module.  Params can be passed to module via hiera.
        include puppet
      }
      /^(Solaris|Illumos)$/ : {
        # No Forge module supports puppet under Solaris yet, farmed out to Nrgillumos module.
        include nrgillumos::puppet_agent
      }
      default: {
        # Here for completeness
      }
    }
  }

}
