# == Class: profile::puppet::puppetca
#
# This class deploys a puppetserver v2.x (aka puppet 4) as a CA, intended to act as companion
# to another machine running the master puppetserver.
#
# Note, when using this class to bootstrap a puppetserver CA, note this class may need to be
# applied up to *three* times to ensure puppet-agent gets its proper "runmode" parameter after
# completion of bootstrap.
#
# More info about this class in the profile module README.md
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.  Note
# that those parameters which lookup a value in hiera and which don't have any default
# specfied below will throw an error if hiera lookup fails.  This behavior is
# intentional.
#
# [*sample_variable*]
#   Explanation of how this variable affects the function of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { 'profile::puppet::puppetca':
#    blahblah => 'blah',
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::puppet::puppetca (
  $bb_username = undef,
  $bb_password = undef,
  $delayed_puppet_runmode = undef,
  $server_autosign_nodes = undef )
{

  include profile::puppet::shared
  include firewall
  require java

  # If puppetca class hasn't yet run to completion, don't enable puppet-agent to avoid possible
  # concurrency issues intefering w/ bootstrap.  Using file_exists() seems necessary to ensure
  # puppet-agent gets its proper "runmode" parameter after bootstrap.
  $delayed_puppet_runmode_real = file_exists("${::settings::hostpubkey}") ? {
    1 => $delayed_puppet_runmode,
    default => 'none'
  }

  # Open port 8140 for incoming puppet agent connections
  firewall { '100 allow puppet agent access':
    dport    => 8140,
    ctstate  => 'NEW',
    proto    => tcp,
    action   => accept,
    chain    => 'INPUT',
  }

  # Install JVM-based puppetserver, configured for CA and directory environments.
  # Additional params can be passed to puppet module via hiera.
  class { '::puppet': 
    server => true,
    runmode => $delayed_puppet_runmode_real,
    server_ca => true,
    server_implementation => 'puppetserver',
    server_external_nodes => '',
    server_environments   => [],
  }
  contain '::puppet'

  # Various puppetserver paths
  $puppet_private_bindir = '/opt/puppetlabs/puppet/bin'
  $puppet_public_bindir = $puppet::puppetca_path
  $puppet_securedir = '/etc/puppetlabs/secure'
  $puppet_confdir = $puppet::dir
  $puppet_environmentpath = $puppet::server_envs_dir

  # TODO: Establish the /etc/puppetmaster/... path from remote NFS mount

  # Grant root user read permission to bitbucket mercurial repos
  # TODO: sort out redundancies b/w this mercurial .hgrc access file and root user RSA key
  if $bb_username {
    $bb_prefix = "https://bitbucket.org/${bb_username}"
    $root_hgrc = '/root/.hgrc'
    concat { $root_hgrc:
      ensure => present,
      owner => 'root',
      group => 'root',
      mode  => '0600',
      before => Class['Puppet'];
    }
    concat::fragment { 'root_hgrc_bitbucket':
      target => $root_hgrc,
      content => "\n[auth]\nbb.prefix = ${bb_prefix}\nbb.username = ${bb_username}\nbb.password = ${bb_password}\n";
    }
  }
  
  # Populate autosign.conf
  if $server_autosign_nodes {
    $server_autosign_nodes_real = join($server_autosign_nodes, "\n")
    file { "${puppet_confdir}/autosign.conf":
      ensure => present,
      content => $server_autosign_nodes_real,
      notify => Service['puppetserver'],
    }
  }

  # Put exported file in shared_dir to signal Puppet CA completion
  @@file { 'puppetca_complete':
    ensure => present,
    path => "${profile::puppet::shared::shared_dir}/puppetca_complete",
    content => 'This is a dummy file used to signal Puppetca class execution completed.  Do not edit.',
    require => Class['Puppet::Server'],
    tag => [ "puppetca_complete" ],
  }
}
