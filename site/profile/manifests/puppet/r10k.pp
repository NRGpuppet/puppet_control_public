# == Class: profile::puppet::r10k
#
# This class deploys the r10k tool for puppet 4.  This class presently assumes CentOS 7.
# This profile::puppet::r10k class is expected to be run on the same node as
# the profile::puppet::puppetserver, i.e. for r10k to provide git control_repo support
# to the puppetserver.
#
# Also, if enabled, this class will deploy a custom cronjob that periodically checks for
# updates from the puppet control repo, having r10k re-deploy if updates found and restarting
# the puppetserver.  This assumes the user under which puppet-agent runs (e.g. root) already
# has read access privileges to the control repo.  I.e. can run "git fetch --dry-run ..."
#
# Note this class can NOT be used to bootstrap r10k, i.e. install the gem and write the
# r10k.yaml config file.  That must be done outside of puppet.
#
# TODO: Refactor this class to do graceful config refresh of puppetserver rather than
# full restart. cf. https://tickets.puppetlabs.com/browse/SERVER-15 and 
# https://tickets.puppetlabs.com/browse/SERVER-96
#
# More info about this class in the profile module README.md
# 
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.  Note
# that those parameters which lookup a value in hiera and which don't have any default
# specfied below will throw an error if hiera lookup fails.  This behavior is
# intentional.
#
# [*sample_variable*]
#   Explanation of how this variable affects the function of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { '::profile::puppet::r10k':
#    r10k_remote_poll => {'hour' => '*', 'minute' => '34'},
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::puppet::r10k (
  $r10k_remote_poll = undef,
  $r10k_remote_poll_random_minute = false,
  ) {

  require puppet
  include profile::puppet::shared

  if $r10k_remote_poll {
    validate_hash($r10k_remote_poll)
  }

  # Install the r10k tool.  Other params, e.g. the remote URL, can be specified
  # via hiera.
  class { 'r10k':}

  # If enabled, add a cronjob script that periodically checks the control repo for changes,
  # launching 'r10k deploy environment' and then puppetserver restart if changes found.
  if $r10k_remote_poll {
    $r10k_remote_poll_hour = has_key($r10k_remote_poll, 'hour') ? { true => $r10k_remote_poll['hour'], default => undef}
    $r10k_remote_poll_month = has_key($r10k_remote_poll, 'month') ? { true => $r10k_remote_poll['month'], default => undef}
    $r10k_remote_poll_monthday = has_key($r10k_remote_poll, 'monthday') ? { true => $r10k_remote_poll['monthday'], default => undef}
    $r10k_remote_poll_weekday = has_key($r10k_remote_poll, 'weekday') ? { true => $r10k_remote_poll['weekday'], default => undef}
    $r10k_basedir = $::r10k::r10k_basedir
    $shared_dir = $profile::puppet::shared::shared_dir
    $puppet_environment = $::environment
    $puppetserver_service = $::puppet::server_implementation ? {
      'puppetserver' => 'puppetserver',
      'master' => $::puppet::server_httpd_service,
      default => undef,
    }

    # If enabled, select random cronjob minute
    if $r10k_remote_poll_random_minute {
      $r10k_remote_poll_minute = fqdn_rand(60)
    }
    else {
      $r10k_remote_poll_minute = has_key($r10k_remote_poll, 'minute') ? { true => $r10k_remote_poll['minute'], default => undef} 
    }
    
    # Note this is RHEL/CentOS specific
    package { 'diffutils':
      ensure => present,
    }
    file { 'r10k_remote_poll_script':
      path => '/usr/local/bin/r10k_remote_poll_script.sh',
      ensure => present,
      mode => '0755',
      content => template('profile/puppet/r10k_remote_poll_script.sh.erb'),
      require => [ Class['R10k'], Package['diffutils'] ],
    }
    cron { 'r10k_remote_poll_cronjob':
      command => File['r10k_remote_poll_script']['path'],
      minute => $r10k_remote_poll_minute,
      hour => $r10k_remote_poll_hour,
      month => $r10k_remote_poll_month,
      monthday => $r10k_remote_poll_monthday,
      weekday => $r10k_remote_poll_weekday,
      require => [ Class['R10k'], File['r10k_remote_poll_script'] ],
    }
    # Finally, run the cronjob script once to create a state file.  Don't stop on error.
    exec { 'exec_r10k_remote_poll_script':
      command => File['r10k_remote_poll_script']['path'],
      require => [ Class['R10k'], File['r10k_remote_poll_script'] ],
      returns => [0, 1],
      creates => "${shared_dir}/r10k_remote_poll_git_state",
    }
  }
}
