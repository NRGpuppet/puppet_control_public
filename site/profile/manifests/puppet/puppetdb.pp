# == Class: profile::puppet::puppetdb
#
# This class deploys a puppetdb v3 (aka puppet 4) backed by PostgreSQL.
# This class presently assumes CentOS 7.  This class can be run on the
# same node as the class profile::puppet::puppetserver, i.e. to have the
# puppetserver and puppetDB housed on the same machine.
#
# More info about this class in the profile module README.md
#
# === Parameters
#
# Parameters used by this class.
#
# NONE
#
# === Variables
#
# Custom variables used by this class.
#
# NONE
#
# === Examples
#
#  class { 'profile::puppet::puppetdb': }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::puppet::puppetdb {

  include profile::puppet::shared
  include firewall
  require java
  require profile::db::postgresql_server

  # Open port 8081 for incoming puppetserver connections
  firewall { '100 allow puppetserver access to puppetdb':
    dport    => 8081,
    ctstate  => 'NEW',
    proto    => tcp,
    action   => accept,
    chain    => 'INPUT',
  }

  # Declare the PuppetDB module, with other params (namely PostgreSQL database credentials)
  # passed via hiera.  Note this module presently requires running on same machine as PostgreSQL
  # server (i.e. same machine that runs postgresql::server class).
  class { '::puppetdb':
    ssl_set_cert_paths => true,
    ssl_listen_address => '0.0.0.0',
    manage_firewall => false,
    manage_dbserver => false,
  }

  # TODO: Devise better permissions on the puppetDB database.ini file, since that includes DB
  # password in plaintext.  Just doing 'chmod 600' on the file causes puppetDB to fail on startup.

  # Once puppetDB is up, make a dummy file into an exported resource for the Puppetserver to collect
  @@file { "puppetdb_exported_resource_${::fqdn}":
    path => "${profile::puppet::shared::shared_dir}/puppetdb_exported_resource_${::fqdn}",
    ensure => present,
    require => Service['puppetdb'],
    tag => [ "puppetdb_exported_resource_${::fqdn}" ],
    content => 'This is a dummy file used to signal PuppetDB availability to Puppetserver.  Do not edit.',
  }
}
