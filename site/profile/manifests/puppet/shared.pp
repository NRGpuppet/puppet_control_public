# == Class: profile::puppet::shared
#
# This is private class containing resources shared by other profile::puppet
# classes.
#
# More info about this class in the profile module README.md
# 
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.  Note
# that those parameters which lookup a value in hiera and which don't have any default
# specfied below will throw an error if hiera lookup fails.  This behavior is
# intentional.
#
# [*sample_variable*]
#   Explanation of how this variable affects the function of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { '::profile::puppet::shared':
#    blah => blah,
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::puppet::shared {

  # Create directory where various files created by this module may be stored.  Can't
  # put things in vardir or statedir, since that varies based on whether this class
  # is invoke by puppetserver or puppet agent apply.

  file { 'profile_shared_dir':
    path => "${::settings::confdir}/profile",
    ensure => directory,
  }
  $shared_dir = File['profile_shared_dir']['path']

}
