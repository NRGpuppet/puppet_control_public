# == Class: profile::db::postgresql_server
#
# This class is a wrapper around the Puppetlabs PostgreSQL module, for installing a PostgreSQL
# server (specifically via the postgresql::server class).
#
# Note that additional parameters can be fed to the postgresql::server and postgresql::globals
# classes, e.g. the version of PostgreSQL to install, via hiera.
#
# More info about this class in the profile module README.md
# 
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.  Note
# that those parameters which lookup a value in hiera and which don't have any default
# specfied below will throw an error if hiera lookup fails.  This behavior is
# intentional.
#
# [*sample_variable*]
#   Explanation of how this variable affects the function of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { '::profile::db::postgresql_server':
#    blah => blah,
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::db::postgresql_server (
  Optional[Hash] $roles_nohash = undef,
  Optional[Hash] $roles = undef,
  Optional[Hash] $dbs = undef,
  Optional[Hash] $extensions = undef,
  Optional[Hash] $config_entries = undef,
  Optional[Hash] $pg_hba_rules = undef,
  Optional[Hash] $pg_ident_rules = undef,
  ) {

  include ::profile::pkg_repos
  
  # Install local PostgreSQL server.  Other params to postgresql::server can be
  # passed via hiera.
  class { '::postgresql::server': }
  Class['Profile::Pkg_repos'] -> Class['Postgresql::Server']

  # Instantiate the DBs, roles, extensions, config entries, etc specified
  if $roles_nohash {
    # These role definitions need a hashed password, so pass through custom type
    create_resources('profile::db::pgsql_role_nohash', $roles_nohash)
  } 
  if $roles {
    create_resources('postgresql::server::role', $roles)
  }
  if $dbs {
    create_resources('postgresql::server::db', $dbs)
  }
  if $extensions {
    create_resources('postgresql::server::extension', $extensions)
  }
  if $config_entries {
    create_resources('postgresql::server::config_entry', $config_entries)
  }
  if $pg_hba_rules {
    create_resources('postgresql::server::pg_hba_rule', $pg_hba_rules)
  }
  if $pg_ident_rules {
    create_resources('postgresql::server::pg_ident_rule', $pg_ident_rules)
  }

}
