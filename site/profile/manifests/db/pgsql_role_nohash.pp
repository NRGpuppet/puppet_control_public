# == Resource type: profile::db::pgsql_role_nohash
#
# This type is a wrapper around the postgresql::role type provided by the Puppetlabs
# PostgreSQL module.  This type permits specifying a 'password' attribute (assuming the
# 'username' attribute is also provided), i.e. without having to specify 'password_hash'. 
# This type will create the 'password_hash' attribute via postgresql_password('username',
# 'password'), and then pass that to postgresql::server::role.
#
# Any other paramters will passed to postgresql::server::role as-is.
#
# Note this custom type will need to closely track that in the Puppetlabs PostgreSQL module:
# https://github.com/puppetlabs/puppetlabs-postgresql/blob/master/manifests/server/role.pp
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# NONE
#
# === Examples
#
#  profile::db::pgsql_role {
#    'myrole':
#      createdb => true,
#      username => 'myusername',
#      password => 'mypassword';
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
define profile::db::pgsql_role_nohash (
  $password         = false,
  $createdb         = false,
  $createrole       = false,
  $db               = $::postgresql::server::default_database,
  $port             = undef,
  $login            = true,
  $inherit          = true,
  $superuser        = false,
  $replication      = false,
  $connection_limit = '-1',
  $username         = $title,
  $connect_settings = $::postgresql::server::default_connect_settings,
  ){

  postgresql::server::role { 
    $name :
      createdb => $createdb,
      createrole => $createrole,
      db => $db,
      port => $port,
      login => $login,
      inherit => $inherit,
      superuser => $superuser,
      replication => $replication,
      username => $username,
      password_hash => postgresql_password($username, $password),
      connect_settings => $connect_settings,
      require => Class['postgresql::server'];
  }

}
