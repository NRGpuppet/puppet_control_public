# == Class: profile::ssl_certs
#
# This class deploys sets of SSL {certificate, chain, and key} files per the
# parameters given.  As the SSL certificate details are specified to this
# class via hash parameter, multiple sets of {SSL cert, chain, key} can be
# deployed.  For each set, pre-existing cert/chain/key files can be referenced,
# self-signed certs can be generated, or the contents of SSL cert files can
# be supplied via parameters.
#
# If the third option is used for any set, it is STRONGLY recommended these
# parameters be supplied via encrypted hiera.
#
# Although this module doesn't explicitly depend on Apache, it is assumed the
# certificate(s) are intended for use with a tool that understands Apache-style
# certificates.
#
# More info about this class in the profile module README.md
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the function of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { 'profile::ssl_certs':
#    certs => {
#      'cert1' => {
#        'cert_file' => {
#          'path' => '/path/to/cert/cert1.pem',
#          'ensure' => 'file',
#          'owner' => 'apache',
#          'mode'  => '0600',
#         },
#        'key_file' => {
#          'path' => '/path/to/cert/key1.pem',
#          'ensure' => 'file',
#          'owner' => 'apache',
#          'mode'  => '0600',
#         },
#        'chain_file' => {
#          'path' => '/path/to/cert/intermediate1.pem',
#          'ensure' => 'file',
#          'owner' => 'apache',
#          'mode'  => '0600',
#         },
#      },
#      'cert2' => {
#        'cert_file' => {
#          'path' => '/path/to/cert/cert2.pem',
#          'ensure' => 'file',
#          'content' => hiera("cert2_contents"),
#          'owner' => 'apache',
#          'mode'  => '0600',
#         },
#        'key_file' => {
#          'path' => '/path/to/cert/key2.pem',
#          'ensure' => 'file',
#          'content' => hiera("key2_contents"),
#          'owner' => 'apache',
#          'mode'  => '0600',
#         },
#        'chain_file' => {
#          'path' => '/path/to/cert/intermediate2.pem',
#          'ensure' => 'file',
#          'content' => hiera("intermediate2_contents"),
#          'owner' => 'apache',
#          'mode'  => '0600',
#         },
#      },
#      'cert3' => {
#        'cert_file' => {
#          'path' => '/path/to/cert/cert3.pem',
#          'ensure' => 'file',
#          'owner' => 'apache',
#          'mode'  => '0600',
#         },
#        'key_file' => {
#          'path' => '/path/to/cert/key3.pem',
#          'ensure' => 'file',
#          'owner' => 'apache',
#          'mode'  => '0600',
#         },
#        'self_signed' => true,
#      },
#    }
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::ssl_certs (
  Optional[Hash] $certs = undef )
{

  # Declare the file resources for each certificate
  $certs.each |$k, $cert| {
    if ($cert['key_file'] and $cert['cert_file'] and $cert['key_file']['path'] and $cert['cert_file']['path']) {

      if $cert['self_signed'] {
        fail("Not implemented yet" )
      }
      else {
        $cert.each |$k, $file_hash| {
          if $file_hash['path'] {
            $file_dirname = dirname($file_hash['path'])
            $resource_to_create = {
              $file_hash['path'] => $file_hash,
            }
            if (!defined(Common::Mkdir_p[$file_dirname])) {
              common::mkdir_p { $file_dirname: }
            }
            create_resources(file, $resource_to_create)
            Common::Mkdir_p[$file_dirname] -> File[$file_hash['path']]
          }
        }
      }
    }
    else {
      fail("Both key_file and cert_file hashes must have path defined for a certificate.")
    }
  }
}
