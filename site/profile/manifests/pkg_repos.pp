# == Class: profile::pkg_repos
#
# The purpose of this class is to provide a convenient, common point of dependency for other
# classes in the profile (or role) module that need package repositories from outside
# the usual channels for a machine's OS.  For example, classes requiring the
# stahnma/epel module should declare a depedency on this class.  Then, this class
# would be declared with '::epel' included in its class_requires parameter in hiera.
#
# Note that putting the "profile::pkg_repos::class_requires" parameter in hiera permits
# deep merging.
#
# Focusing such package repo dependencies into this shared class allows for the list of 3rd
# party classes providing such repos (e.g. stahnma/epel) to be parametrized in hiera, and
# thus customizable for OS, OS revision, etc.
#
# More info about this class in the profile module README.md
#
# === Parameters
#
# Parameters used by this class.
#
# NONE.
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*class_requires*]
#   This class expects to retrieve an array of class names via 
#   hiera_array('profile::repos::class_requires').  Those classes will then be
#   "require"-d.
#
# === Examples
#
# In hiera YAML:
#
#  --
#  profile::pkg_repos::class_requires:
#    - "::epel"
#    - "::yum"
#
# In code:
#
#  class { 'profile::pkg_repos': }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::pkg_repos {

  $class_requires = hiera_array('profile::pkg_repos::class_requires', undef)

  # Declare class dependencies
  if $class_requires {
    $class_requires.each |$c| {
      require "${c}"
    }
  }
}
