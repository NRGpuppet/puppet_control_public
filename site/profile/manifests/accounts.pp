# == Class: profile::accounts
#
# This manifest is a wrapper class for the deric/accounts module.  This class exists solely
# to permit deeper merging on hiera() lookups of the 'users' and 'groups' hashes, which it
# will then pass to the accounts module.
#
# That is, this class is intended to permit specifying and overriding those hashes at
# multuple levels of the hiera hierarcy.
#
# The default 'priority lookup' behavior by Puppet's automated parameter lookup is just
# inadequate for this instance, as any overriding definition of the hash accounts::users,
# i.e. at a higher level of hierarchy, would not merge with non-overlapping items in that
# same hash (e.g. other users) defined at lower levels.  This default behavior is badly
# counter-intuitive, and so this class seeks to work-around Puppetlabs' willful obstinance
# on the subject.
#
# Cf. https://docs.puppetlabs.com/hiera/3.0/puppet.html#automatic-parameter-lookup
#
# More info about this class in the profile module README.md
#
# === Parameters
#
# NONE
#
# === Variables
#
# Below are variables this class will look up in hiera().
#
# *profile::accounts::users*
#  A hash of users accounts to manage, to be passed to the deric/accounts module
#
# *profile::accounts::groups*
#  A hash of user groups to manage, to be passed to the deric/accounts module
#
# === Examples
#
#  class { '::profile::accounts': }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile::accounts {

  # Retrieve hashes from hiera using deep merging behavior
  $users = hiera_hash('profile::accounts::users', undef)
  $groups = hiera_hash('profile::accounts::groups', undef)

  case $::osfamily {
    /^(Solaris|Illumos)$/ : {
      # Solaris / OmniOS have an *additional* wrapper class
      class { '::nrgillumos::accounts':
        users => $users,
        groups => $groups,
      }
    }
    default: {
      class { '::accounts':
        users => $users,
        groups => $groups,
      }
    }
  }


}
