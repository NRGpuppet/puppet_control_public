profile
=======

####Table of Contents

1. [Overview - What is the Profile module?](#overview)
2. [Usage - The classes and parameters available for configuration](#usage)
3. [Advanced Usage - Advanced use cases, e.g. deploying a Puppetserver](#advanced-usage)

Overview
--------

The Profile module includes classes needed for bootstrapping and provisioning machines
from this control repo.  For example, this module includes a base class shared by
all machines, classes necessary to bootstrap and maintain a Puppetserver, and
so forth.

This module is intended to be used in conjunction with a companion Role module, whereby
nodes will include classes in the Role module that include includes classes in this
module.  This seperation allows classes in the Role module to take more human-readable
names, e.g. Maven server, build server, load balancer, while classes in this Profile
module focus more on the layers of the actual application stack, e.g. nginx server, JVM,
postgresql server.

Additionally, this separation allows for ease in changing and editing application layers.
For example, switching from Apache to Nginx could be done by editing a single line in a 
role class, instead of editing dozens of node manifests for all the machines assigned to
that role.

Here are the conventions preferred for the Profile and Role modules:

1. Classes in the Role module only include classes in the Profile module.  Classes from
additional modules would be included in Profile module classes.
1. The Role module takes no parameters, only the Profile module.  Likewise, only classes
in the Profile module do hiera lookups.
1. A slightly different role should be a different role class entirely.  E.g.
role::puppet::puppetserver_int_db and role::puppet::puppetserver_ext_db.

Please also refer to the Role module [README](../role/README.md).

Finally, cf. [The origins of roles and profiles](http://www.craigdunn.org/2012/05/239/)

Usage
-----

module, which presently is already done via environment-wide manifests/site.pp. So,
classes included in hiera or puppet code would be in addition to the base role.

Example in hiera:

```
classes:
 - role::xnat::xnat
 - ... additional classes

# Puppet module params for puppet-agent
puppet::runmode: service
puppet::puppetmaster: (FQDN of master Puppetserver)
puppet::ca_server: (FQDN of Puppetserver CA, if not the master Puppetserver)

# Hiera params for XNAT module
xnat::db_name: xnatdb
xnat::db_user: xnatuser
xnat::db_host: localhost
xnat::db_pass: secure_passwod

# Hiera params for other modules
...
```

## Classes included in this module:

### profile::base
Base class shared by all machines.  This class is included by the base role, i.e.
so that all machines run this class.

**Parameters within `profile::base`:**
TBD

### profile::ssl_certs
This class deploys sets of {SSL certificate, chain, and key} per its parameters.

**Parameters within `profile::ssl_certs`:**
TBD

### profile::puppet::agent
Installs and manages the puppet-agent, or does nothing if profile::puppet::puppetserver
or profile::puppet::puppetca classes included as well.  This class is also included by
role::init, i.e. so that all machines run this class.

**Parameters within `profile::puppet::agent`:**
TBD

### profile::puppet::puppetserver
Installs and manages the puppetserver.  This class can not be run on the same node
as profile::puppet::puppetca.

**Parameters within `profile::puppet::puppetserver`:**
TBD

### profile::puppet::hiera
Installs and manages the hiera tool for a puppetserver.

**Parameters within `profile::puppet::hiera`:**
TBD

### profile::puppet::r10k
Installs and manages the r10k tool for a puppetserver.

**Parameters within `profile::puppet::r10k`:**
TBD

### profile::puppet::puppetca
Installs and manages a puppetserver CA, as companion to a master puppetserver.  This class
can not run on the same node as profile::puppet::puppetserver.

**Parameters within `profile::puppet::puppetca`:**
TBD

### profile::puppet::puppetdb
Installs and manages a PostgeSQL-backed puppetDB host.

**Parameters within `profile::puppet::puppetdb`:**
TBD

### profile::puppet::shared
This is a private shared class used by other profile::puppet classes.

### profile::monitoring::icinga2_server
Installs the Icgina2 monitoring server and its web UI.  Still a WIP.

**Parameters within `profile::monitoring::icinga2_server`:**
TBD


Advanced Usage
--------------

TBD
