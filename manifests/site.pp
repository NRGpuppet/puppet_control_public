# $environment/manifests/site.pp

###############################################################################
#####
#####   Environment site manifest.
#####
###############################################################################

# PUPPETVERSION
case $::puppetversion {
  /(^4\..*|^5\..*)/: {
    # puppet v4, v5 code

    notify {"Detected puppetversion ${::puppetversion}." :}

    # The code below sets some custom facts via top-level variables, namely project and location.
    # Preferred practice may be to use a full ENC like TheForeman.  Specificy facts locally on nodes
    # under /etc/facter is not desired for production use.

    # If not specified already via custom fact, set top-level "project" fact from hiera.
    if (! $::project) {
      $project = hiera('project', 'default')
    }

    # Similarly, set default fact location = MyLocation
    if (! $::location) {
      $location = hiera('location', 'MyLocation')
    }
    
    # First, include the base role class, which performs ntp install and other tasks
    include role

    # Next, if this server is assigned a role via custom "role" fact or in hiera, include that
    # role (i.e. assuming role is a full puppet class name).
    if $::role {
      include $::role
    }
    else {
      $role = hiera('role', undef)
      if $role {
        include $role
      }
    }

    # Next, import additional classes defined in hiera
    hiera_include('classes')

    # Finally, install the puppet agent
    include role::puppet::agent
  }
  
  default: {
    # We require Puppet v4+
    fail ( "This environment requires Puppet v4+." )
  }
}

# Default file resource parameters
File { 
  owner => 'root',
  group => 'root',
  backup => main,
  ignore => '.svn',
}

# Default exec resource parameters
Exec {
  path => ["/bin", "/sbin", "/usr/bin", "/usr/sbin"],
  logoutput => on_failure,
}

# Specify default remote filebucket (i.e. stored on presently configured puppetserver)
filebucket { main:
  path => false,
}

# Define default schedules
schedule { 'nightly':
  period => daily,
  range  => "2-4",
}
