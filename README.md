# NRG Puppet Control Repo

## Overview

This is the control repo for NRG Puppet infrastructure (for the Puppet v4.x platform as of
this writing).

This environment controls all of your environments (dev, staging, production).  Branches of this repo
correspond to Puppet environments, and there must be a branch named "production".  Besides that, you
may have as many additional branches/environments as you need, e.g. dev2, dev3, staging_first,
staging_second, with the caveat that branch names may only include alphanumeric chars and
underscores (i.e. no hyphens, punctuation, whitespace).

In addition, this repo contains Vagrant support for spinning up a Puppetserver and associated
VMs for quick tests.

This repo is based on this one found on github, and it is also following general Puppet v4
conventions for control repos.
https://github.com/terrimonster/puppet-control

## Usage Instructions

### Puppet Control repo usage
This is the conventional workflow for using this Puppet control repo with r10k and directory
environments:

0. Check out a copy of this repo to your workstation.
1. Edit the contents of that working copy, e.g. the Puppetfile, a base class, or a hiera data file.
Test that change locally with Vagrant or some other method.
2. Push your changes to the relevant branch of this repo, preferably not immediately to the
"production" branch but rather to "dev."
3. On the relevant Puppetserver machine, verify the changes you just committed to the the "dev"
environment is sound.  I.e., have the Puppetserver provision a test node in that environment.
4. Once the changes have been verified in the "dev" environment, merge the relevant git commits
into the "production" branch of this repo to propagate the changes to your "production" environment
(including all nodes in that environment).
5. Alternately, you can create additional branches/environments to establish a multi-stage workflow
into production, e.g. dev -> staging -> production.

Cf. [Puppet control repo with r10k](https://techpunch.co.uk/development/how-to-build-a-puppet-repo-using-r10k-with-roles-and-profiles)

### Vagrant Usage
This repo can stand up the following Vagrant VMs.  Note that all machines together require ~5GB of RAM.

| Name         | Description                   | Address        | RAM  |
| ------------ | ----------------------------- | -------------- | ---- |
| xmaster      | The Puppetserver (aka Master) | 192.168.137.10 | 2GB  |
| xpuppetdb    | The Puppet DB                 | 192.168.137.12 | 1GB  |
| xpuppetca    | The Puppet CA                 | 192.168.137.13 | 1GB  |
| xagent       | Example agent (unclassified)  | 192.168.137.14 | 512M |
| xagentomnios | OmniOS agent (unclassified)   | 192.168.137.15 | 1GB  |

#### Install VirtualBox/Vagrant
This repo has been tested with VirtualBox 5.0 and Vagrant 1.7.4.  Both tools must be installed on your
workstation prior to using this repo.  Note you may have to patch vagrant per [issue #5070](https://github.com/mitchellh/vagrant/issues/5070#issuecomment-75738232).

#### Summary of procedure

1. Push changes changes in working copy of this repo to "dev" branch (TODO: revise)
2. Bring up Vagrant nodes
3. Experiment/verify
4. Push verified changes to "production" branch.

**Bring up all the nodes in the Vagrant environment, recommended one at a time, i.e.:**
```
vagrant up xpuppetca; vagrant up xmaster; vagrant up xpuppetdb
```
This will take some time to provision.  Also, it is recommended to bring up machines one at a time
since bootstrapping a distributed Puppetserver setup (e.g. CA + Puppetserver + puppetDB) is unavoidably
fragile.

Furthermore, the order in which machines are brought up in a distributed Puppetserver setup must be as 
follows, due to cross-node bootstrapping dependencies:
xpuppetca -> xmaster -> xpuppetdb -> xagent (and any additional agent nodes)

Note that for nodes besides xmaster and xpuppetca, i.e. puppet agent nodes, you can specify a custom puppet role to run on launch.  This assumes a functioning Puppetserver is already running.
```
ROLE="role::monitoring::icinga2_server" vagrant up xagent
```

Once everything is provisioned as you need it, you can ssh into specific machines like the Puppet Master:
```
vagrant ssh xmaster
```
You will be logged in as user vagrant. Please sudo to root if you need to run puppet.

### AWS Usage
The script provision/aws_provision.sh is provided for provisioning AWS EC2 instances.  More details
TBD.

### Scalr Usage
The script provision/scalr_provision.sh is provided for provisioning AWS EC2 instances using the Scalr
dashboard tool.

For Route53 integration in Scalr, you will also need this webhook request handler:
https://bitbucket.org/NRGpuppet/scalr_route53_updater

Further details TBD.

## Files/Directories

### Puppetfile
r10k needs this file to figure out what component modules you want from the Forge and from
custom NRG git repos. The result is a modules directory on the Puppet Master containing all
the modules specified in this file, for each environment/branch. The modules directory is listed
in environment.conf's modulepath.

### environment.conf
This file controls puppet's directory environment settings.  Note that we're not using
$basemodulepath as of this writing.  We're just pointing $modulepath to the site and modules
directories here.

Cf. [Config Files: environment.conf](https://docs.puppetlabs.com/puppet/latest/reference/config_file_environment.html)

### Vagrantfile
This file dictates the basics of how Vagrant will spin up VM. Please do not edit this file
unless you really know what you're doing.

### vagrant.yml
This file gives instructions to Vagrantfile regarding what Vagrant box you want to use, what
virtual machines are available for provisioning, what memory and network options are, and it
specifies bootstrapping for Vagrant boxes (including order when launching multiple VMs).  We're
using a plain CentOS 7.0 box by default, and you could change that in this file.

### hiera
This directory contains your hiera directory tree and data files.  Please refer to the hiera
[README](hiera/README.md).

### manifests
This directory would contain the environment-wide site.pp.  If your Puppet Master is not configured
to point to a global site.pp manifest (which isn't included in this repo), then it will want to find
the environment-wide manifest here.

### site
This directory contains the Profile and Role modules needed to bootstrap and provision machines
from this control repo.  Specifically, these modules contain the base class, role classes, and
dependent subclasses, along with classes needed to bootstrap/manage the Puppetserver itself.  Please
refer to the Profile module [README](site/profile/README.md) and the Role module [README](site/role/README.md).

This directory is specified as a modulepath in environment.conf

Cf. [The origins of roles and profiles](http://www.craigdunn.org/2012/05/239/)

### modules
The modules directory would be where r10k deploys modules retrieved from Forge or from other NRG
repos.  However, this directory would *only* exist in the checked out copy of this control repo
on the Puppet Master and normally *not* on the copy on your workstation.  This directory is excluded
from any accidental git commits via the gitignore file.

This directory is also specified as a modulepath in environment.conf

### provision
This directory contains provisioning scripts for Vagrant and AWS, along with platform-independent
bootstrapping scripts (i.e. will run on Vagrant, AWS, vSphere, etc).

### provision/vagrant_provision.sh
This is the provisioning script specified in vagrant.yaml, which Vagrant launches on new VMs. This
script assumes a CentOS 7 VM.  Although this script is only intended to run under a 
Vagrant/Virtualbox environment, it can be used as reference in provisioning machines under other
environments, e.g. vSphere, bare metal.  This provisioning script will run one of the bootstrap
scripts below.

### provision/aws_provision.sh
This is the provisioning script intended for EC2 instances launched under AWS.  This script
assumes a CentOS 7 machine.  This provisioning script will run one of the bootstrap scripts
below.  More details TBD.

### provision/scalr_provision.sh
This is the provisioning script intended for AWS EC2 instances launched via the Scalr dashboard
tool.  This script assumes a CentOS 7 machine.  This provisioning script will run one of the
bootstrap scripts below. More details TBD.

### provision/bootstrap_puppetserver.sh
This is the bootstrapping script launched by one of the *_provision.sh scripts above on the
puppetserver machine.  This script installs the puppetserver package and dependencies, including
those for a dedicated puppetserver CA.  This script assumes a CentOS 7 VM.

Additionally, this script may be launched manually (or via some other automation) on a suitably
provisioned machine in some other environment, e.g. vSphere, bare metal.

### provision/bootstrap_agent.sh
This is the bootstrapping script launched by one of the *_provision.sh scripts above on any
other machine, which is assumed to be a puppet node.  This script installs the puppet-agent
package and dependencies, and then it launches the puppet-agent to try to retrieve
configuration from a Puppetserver.  This script assumes a CentOS 7 VM.

Additionally, this script may be launched manually (or via some other automation) on a suitably
provisioned machine in some other environment, e.g. vSphere, bare metal.

### provision/params.sh
This script defines variables shared by the provision and bootstrap scripts listed above.

### provision/keys/ssh
You can place RSA key pairs in this directory for Vagrant to install for the root user on Vagrant VMs.
For example, this key pair could grant the VM access to this puppet_control repo.  Specifically, the
provision scripts look for the following keys:

* provision/keys/ssh/vagrant_root.rsa
* provision/keys/ssh/vagrant_root.rsa.pub

The contents of this directory are excluded from git commits via gitignore file.

### provision/keys/eyaml
You can place a PCKS#7 key pair in this directory for Vagrant to install as the Hiera encryption
keys on the xmaster and xpuppetca VM's.  This will let those VMs read encrypted Hiera files during
bootstrap.  Specifically, the provision scripts look for the following keys:

* provision/keys/eyaml/private_key.pkcs7.pem
* provision/keys/eyaml/public_key.pkcs7.pem

The contents of this directory are excluded from git commits via gitignore file.

### provision/keys/ssl
TBD install puppet SSL certs.

The contents of this directory are excluded from git commits via gitignore file.

## Other Notes
This repo makes use of Greg Sarjeant's [data-driven-vagrantfile](https://github.com/gsarjeant/data-driven-vagrantfile).

### Custom Vagrant Boxes

#### Custom CentOS Boxes
The stock CentOS boxes available from Puppet Labs will generally require long yum updates upon
launching each VM.  You can save time by having vagrant.yaml point to a customized box with
packages pre-installed.  That is, create a dummy VM with the stock CentOS 7 box, install whatever
necessary packages, and then export that VM into a new box.

For example, customizations performed as vagrant user on a dummy CentOS 7 VM "xdummy," followed
by reboot:
```
sudo yum -y clean all ; yum -y update
sudo rpm -Uvh https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm
sudo yum -y install puppet-agent
sudo yum -y install git curl
sudo yum -y install chrony openssl-devel libgfortran compat-libgfortran-41 nss_ldap openldap openldap-clients iotop latencytop dstat perf powertop dropwatch ipstate tcpdump wireshark mutt dos2unix unix2dos cmake compat-gcc-44 rpmdevtools emacs-gnuplot emacs-nox expect screen symlinks tree tcl tk tcl-devel tk-devel vim-X11 gnome-disk-utility telnet expat yum-plugin-downloadonly gedit ImageMagick ruby-devel httpd-devel net-tools perl vim-enhanced realmd mercurial mlocate
sudo yum -y erase biosdevname NetworkManager
sudo yum -y groupinstall --setopt=group_package_types=optional,default,mandatory "Compatibility libraries"
sudo yum -y groupinstall --setopt=group_package_types=optional,default,mandatory "Debugging Tools"
sudo yum -y groupinstall --setopt=group_package_types=optional,default,mandatory "Internet Browser"
sudo yum -y groupinstall --setopt=group_package_types=optional,default,mandatory "Java Platform"
sudo yum -y groupinstall --setopt=group_package_types=optional,default,mandatory "X Window System"
sudo yum -y groupinstall --setopt=group_package_types=optional,default,mandatory "Emacs"
sudo yum -y groupinstall --setopt=group_package_types=optional,default,mandatory "Graphics Creation Tools"
sudo reboot ; exit
```
The shared /vagrant mount will probably fail after reboot, so install the latest VirtualBox guest
 additions (v5.x as of this writing) to resolve this, and then logout:
```
sudo mount /dev/cdrom /mnt
sudo /mnt/VBoxLinuxAdditions.run --nox11
exit
```
Back in the control repo, export the dummy VM into a new box:
```
vagrant package --output custom-nrg-puppetlabs-centos-7.2-64-cm.box --base xdummy
vagrant box add custom-nrg-puppetlabs-centos-7.2-64-cm custom-nrg-puppetlabs-centos-7.2-64-cm.box
rm custom-nrg-puppetlabs-centos-7.2-64-cm.box
```

#### Custom OmniOS Boxes
At the time of writing, the pre-built Vagrant boxes for OmniOS aren't 100% functional, requiring
building custom Vagrant boxes.

Besides Vagrant, you will need to install Packer to build OmniOS boxes:
https://www.packer.io/intro/getting-started/setup.html

Next, checkout the omnios-packer git repo mention on this page:
http://omnios.omniti.com/wiki.php/VagrantBaseboxes

In the checked out copy, update the ISO checksum in templates/omnios-r151014.json
```
$ git diff templates/omnios-r151014.json
diff --git a/templates/omnios-r151014.json b/templates/omnios-r151014.json
index b244037..a12d53b 100644
--- a/templates/omnios-r151014.json
+++ b/templates/omnios-r151014.json
@@ -21,7 +21,7 @@
       "name": "omnios-r151014",
       "guest_os_type": "OpenSolaris_64",
       "iso_url": "http://omnios.omniti.com/media/OmniOS_Text_r151014.iso",
-      "iso_checksum": "7e50479eaa70004d2d47fc1b7eadc330",
+      "iso_checksum": "59643db5b438a16dc8e6a20e8c09159c",
```

Then build the box and import it into vagrant:
```
packer build templates/omnios-r151014.json
vagrant box add --name 'custom-nrg-omnios-r151014' boxes/omnios-r151014.box
```
