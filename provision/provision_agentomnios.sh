#!/bin/bash

# Bootstrap a puppet agent node under OmniOS, for vagrant and otherwise.

if [[ $HOSTNAME =~ vagrant ]] ; then
    SCRIPT_PATH='/vagrant/provision'
else
    SCRIPT=$(readlink -f $0)
    SCRIPT_PATH=`dirname $SCRIPT`
fi
source ${SCRIPT_PATH}/params.sh || (echo "Can't find params.sh" && exit 1)
CWD=$(pwd)

# Overide some params specific to OmniOS
PUPPET_BINDIR="/opt/csw/bin"

# Ruby Gems update from: https://github.com/rubygems/rubygems/releases
RUBYGEMS_VERSION="2.0.15"
RUBYGEMS_UPDATE="rubygems-update-${RUBYGEMS_VERSION}.gem"
RUBYGEMS_UPDATE_SOURCE="https://github.com/rubygems/rubygems/releases/download/v${RUBYGEMS_VERSION}"

# Function to install OpenCSW packages
installpackage() {
    package="$1"
    /usr/bin/pkginfo|grep -q $package &>/dev/null
    if [ "$?" -ne "0" ]; then
        echo "Installing $package"
        /opt/csw/bin/pkgutil -y -i $package
    else
        echo "Package $package already installed.  Skipping."
    fi
}

# Append to PATH, so puppet agent run below finds ruby
export PATH=${PATH}:/usr/local/bin:${PUPPET_BINDIR}

# Add host entries for other vagrant boxes
if [[ $HOSTNAME =~ vagrant ]] ; then
    cat <<HOSTS >> /etc/hosts
192.168.137.10 xmaster.vagrant.vm xmaster puppet
192.168.137.12 xpuppetdb.vagrant.vm xpuppetdb puppetdb
192.168.137.13 xpuppetca.vagrant.vm xpuppetca puppetca
192.168.137.14 xagent.vagrant.vm xagent agent
192.168.137.15 xagentomnios.vagrant.vm xagentomnios agentomnios
HOSTS
fi

# Update DNS config, if specified
if [ "${RESOLV_CONF}" ]; then
    echo "${RESOLV_CONF}" > /etc/resolv.conf
fi

# Install OpenCSW
pkginfo CSWpkgutil 2> /dev/null
if [ "$?" -ne "0" ]; then
    cat > /tmp/pkgadd_opencsw <<PKGADD
mailinstance=overwrite
partial=nocheck
runlevel=nocheck
idepend=nocheck
rdepend=nocheck
space=nocheck
setuid=nocheck
conflict=nocheck
action=nocheck
basedir=default
PKGADD
    echo "all\n\n" | pkgadd -d http://get.opencsw.org/now -a /tmp/pkgadd_opencsw
fi

# Enable OpenCSW mirror, if specified
if [ "${OPENCSW_MIRROR}" ]; then
    echo "mirror=${OPENCSW_MIRROR}" >> /etc/opt/csw/pkgutil.conf
fi

# Install package dependencies
pkg install archiver/gnu-tar
installpackage cswpki
installpackage CSWcoreutils
installpackage CSWruby20

# Update rubygems
current_gems_version=`/opt/csw/bin/gem --version`

if [ "$current_gems_version" != "${RUBYGEMS_VERSION}" ]; then
    wget ${RUBYGEMS_UPDATE_SOURCE}/${RUBYGEMS_UPDATE}
    /opt/csw/bin/gem install --local ${RUBYGEMS_UPDATE} && rm ${RUBYGEMS_UPDATE}
fi

# Download and install puppet-agent if not already present
if [ ! -f "${PUPPET_BINDIR}/puppet" ]; then
    /opt/csw/bin/gem install --no-rdoc --no-ri puppet
fi

# Point puppet agent to puppet master/CA
[ ! "${PUPPET_CA}" ] && PUPPET_CA=${PUPPET_MASTER}
mkdir -p ${PUPPET_CONFDIR}
cat <<PUPPET >> ${PUPPET_CONFDIR}/puppet.conf
[main]
    ca_server = ${PUPPET_CA}
[agent]
    server = ${PUPPET_MASTER}
    environment = ${PUPPET_ENVIRONMENT}
PUPPET

# This works around error on agent nodes when using non-production environment
# Ref: https://tickets.puppetlabs.com/browse/PUP-4954
mkdir -p ${PUPPET_ENVIRONMENTPATH}/${PUPPET_ENVIRONMENT}

# If present, copy in any custom RSA key for Vagrant root user.
mkdir -p ~/.ssh
chmod 700 ~/.ssh
if [[ $HOSTNAME =~ vagrant ]] && [ -f /vagrant/provision/keys/ssh/vagrant_root.rsa ]; then
    cp /vagrant/provision/keys/ssh/vagrant_root.rsa ~/.ssh/id_rsa
    chmod 0600 ~/.ssh/id_rsa
fi

# Install control repo RSA key
if [ "${PUPPET_CONTROLREPO_PRIVKEY}" ]; then
    echo "${PUPPET_CONTROLREPO_PRIVKEY}" > ~/.ssh/controlrepo_rsa
    chmod 0600 ~/.ssh/controlrepo_rsa
    cat <<SSHCONFIG >> ~/.ssh/config
Host ${PUPPET_CONTROLREPO_HOSTNAME}
	HostName ${PUPPET_CONTROLREPO_HOSTNAME}
	PreferredAuthentications publickey
	IdentityFile ~/.ssh/controlrepo_rsa
SSHCONFIG
fi

# Populate known_hosts file
if [ "${PUPPET_CONTROLREPO_PUBKEY}" ]; then
    echo "${PUPPET_CONTROLREPO_PUBKEY}" >> ~/.ssh/known_hosts
else
    ssh-keyscan -t rsa ${PUPPET_CONTROLREPO_HOSTNAME} >> ~/.ssh/known_hosts 2>/dev/null
fi

# Install git and curl if not already present
installpackage CSWgit
installpackage CSWcurl

# Finally, run puppet agent on myself
( PATH=${PATH}:/usr/local/bin:${PUPPET_BINDIR} ${PUPPET_BINDIR}/puppet agent -t --verbose --waitforcert=120 --environment=${PUPPET_ENVIRONMENT} ; echo "Result code: $?" ) | tee -a /var/log/puppet_deploy.log
