#!/bin/bash

# Provisioning script for CentOS 7 Puppet machines under Scalr + AWS.
#
# This script is intended to be able to run standalone, but with these global
# environment variables already populated by Scalr.  Except where noted, these
# custom variables must be defined.
#
# PUPPET_MASTER = FQDN or IP address of puppet master
# PUPPET_CA = for now, same as PUPPET_MASTER
# PUPPET_CONTROLREPO_URL = "git@bitbucket.org/NRGpuppet/puppet_control_public"
# PUPPET_CONTROLREPO_HOSTNAME = "bitbucket.org"
# PUPPET_CONTROLREPO_PRIVKEY = private RSA key granting access to the control_repo
# PUPPET_CONTROLREPO_PUBKEY = control_repo public RSA key, for known_hosts file, optional
# PUPPET_LOCATION = a location tier for hiera, optional
# PUPPET_PROJECT = a project tier for hiera, optional
# PUPPET_LOCATION = a location tier for hiera, optional
# R53_ZONE = Route53 hosted zone, optional. Example: customdomain.com
# R53_NAME = Route53 A record name, optional.  Example: customhostname
#
# These additional, optional environment variables can be used to pre-populate a keypair
# for hiera EYAML content.
#
# PUPPET_EYAML_PUBKEY = public PKCS7 EYAML key
# PUPPET_EYAML_PRIVKEY = private PKCS7 EYAML key
#
# Script parameters:
#
# %puppet_role% = role class this machine will have, required.  For example
#   role::puppet::postgres_server.  It is prefered to only point this param to a
#   class in the Role module in this control_repo.  If %puppet_role% = no_role,
#   this script will update puppet.conf to point to PUPPET_MASTER and PUPPET_CA,
#   enable the puppet agent (i.e. for periodic cron run), but NOT launch any
#   a bootstrap script to actually run the puppet agent. 
#
# This script will attempt to pull down the puppet_control repo specified, and then
# launch bootstrap scripts retrieved from that repo, per the %puppet_role% script
# parameter.
#
# If both R53_ZONE and SCALR_EXTERNAL_IP are not blank, this script will set this
# machine's hostname to one of the following:
#  - R53_NAME.R53_ZONE, if R53_NAME is set and SCALR_INSTANCE_INDEX=1
#  - R53_NAME-SCALR_INSTANCE_INDEX.R53_ZONE, if SCALR_INSTANCE_INDEX>1
#  - SCALR_CLOUD_SERVER_ID.R53_ZONE, if R53_NAME is blank
#
# Else, if R53_ZONE is blank but SCALR_EXTERNAL_IP is set, this script will set
# the hostname to Amazon's ec2-*.compute-1.amazonaws.com form.  Note such hostnames
# on instances w/o Elastic IP will become out of date if instance is stopped/started.
#
# Please note this script DOES NOT actually alter any Route53 records. It assumes DNS
# has already been updated so that a hostname like R53_NAME.R53_ZONE or
# SCALR_CLOUD_SERVER_ID.R53_ZONE correctly resolves.  Here is a simple Scalr webhook
# request handler update Route53 recods:
# https://bitbucket.org/NRGpuppet/scalr_route53_updater
#
# Further Reference:
# https://scalr-wiki.atlassian.net/wiki/display/docs/Using+Puppet+with+Scalr

# Update the machine's hostname
if [ "${SCALR_EXTERNAL_IP}" ] && [ "${R53_ZONE}" ] && [ "${R53_NAME}" ]; then
    if [ "${SCALR_INSTANCE_INDEX}" == '1' ]; then
        PUBLIC_HOSTNAME="${R53_NAME}"
    else
        PUBLIC_HOSTNAME="${R53_NAME}-${SCALR_INSTANCE_INDEX}"
    fi
    PUBLIC_HOSTNAME="${PUBLIC_HOSTNAME}.${R53_ZONE}"
    NEW_DOMAIN="DOMAIN=\"${R53_ZONE} compute-1.amazonaws.com\""
    HOSTNAME_CHANGED=1
elif [ "${SCALR_EXTERNAL_IP}" ] && [ "${R53_ZONE}" ]; then
    PUBLIC_HOSTNAME="${SCALR_CLOUD_SERVER_ID}.${R53_ZONE}"
    NEW_DOMAIN="DOMAIN=\"${R53_ZONE} compute-1.amazonaws.com\""
    HOSTNAME_CHANGED=1
elif [ "${SCALR_EXTERNAL_IP}" ]; then
    PUBLIC_HOSTNAME=$(echo "${SCALR_EXTERNAL_IP}" | sed s'/\./\-/g')
    PUBLIC_HOSTNAME="ec2-${PUBLIC_HOSTNAME}.compute-1.amazonaws.com"
    NEW_DOMAIN="DOMAIN=compute-1.amazonaws.com"
    HOSTNAME_CHANGED=1
fi
if [ "${HOSTNAME_CHANGED}" ]; then
    echo ${NEW_DOMAIN} >> /etc/sysconfig/network
    hostnamectl set-hostname ${PUBLIC_HOSTNAME} --static
    hostnamectl set-hostname ${PUBLIC_HOSTNAME}
    service network restart
    systemctl restart rsyslog
fi

# Install control repo RSA key
mkdir -p ~/.ssh
chmod 0700 ~/.ssh
if [ "${PUPPET_CONTROLREPO_PRIVKEY}" ]; then
    echo "${PUPPET_CONTROLREPO_PRIVKEY}" > ~/.ssh/controlrepo_rsa
    chmod 0600 ~/.ssh/controlrepo_rsa
    cat <<SSHCONFIG >> ~/.ssh/config
Host ${PUPPET_CONTROLREPO_HOSTNAME}
	HostName ${PUPPET_CONTROLREPO_HOSTNAME}
	PreferredAuthentications publickey
	IdentityFile ~/.ssh/controlrepo_rsa
SSHCONFIG
fi

# Populate known_hosts file
if [ "${PUPPET_CONTROLREPO_PUBKEY}" ]; then
    echo "${PUPPET_CONTROLREPO_PUBKEY}" >> ~/.ssh/known_hosts
else
    ssh-keyscan ${PUPPET_CONTROLREPO_HOSTNAME} >> ~/.ssh/known_hosts 2>/dev/null
fi

# Yum update, to appease yum_update repo
yum clean all
# don't do yum update, can freeze and block machine launch
#yum -y update | tee -a /var/log/puppet_deploy.log
mkdir -p /var/spool/check_yum_update
echo "$(( $(/bin/date +%s) / 86400 ))" > /var/spool/check_yum_update/last_update

# Install git and curl if not already present
yum -y install git curl

# Retrieve updated control_repo
PUPPET_CONTROLREPO_URL_TRIMMED=$(echo ${PUPPET_CONTROLREPO_URL} | sed s/'ssh:\/\/'/''/g)
rm -rf ~/puppet_control
git clone ${PUPPET_CONTROLREPO_URL_TRIMMED} ~/puppet_control

# Pull in default provision params
SCRIPT_PATH="${HOME}/puppet_control/provision" 
source ${SCRIPT_PATH}/params.sh || (echo "Can't find params.sh" && exit 1)
CWD=$(pwd)

# A reasonable PATH
echo "export PATH=${PATH}:/usr/local/bin:${PUPPET_BINDIR}" >> /etc/bashrc

# Install Chrony to force time sync
# Note these commands are CentOS 7 specific
yum install -y chrony
systemctl enable chronyd
systemctl start chronyd

# Download and install puppet-agent if not already present
if [ ! -d "${PUPPET_BINDIR}" ]; then
    rpm -Uvh "${PUPPET_RELEASE_RPM_EL7}"
    yum -y install puppet-agent
fi

# Point puppet agent to puppet master/CA
[ ! "${PUPPET_CA}" ] && PUPPET_CA=${PUPPET_MASTER}
cat <<PUPPET >> ${PUPPET_CONFDIR}/puppet.conf
[main]
    ca_server = ${PUPPET_CA}
[agent]
    server = ${PUPPET_MASTER}
PUPPET

# This works around error on agent nodes when using non-production environment
# Ref: https://tickets.puppetlabs.com/browse/PUP-4954
mkdir -p ${PUPPET_ENVIRONMENTPATH}/${PUPPET_ENVIRONMENT}

# Update EYAML keys if I am puppetserver or puppet CA
if [ "%puppet_role%" == 'role::puppet::puppetca' ] || [[ "%puppet_role%" =~ 'role::puppet::puppetserver' ]]; then

    mkdir -p ${PUPPET_KEYSDIR}
    # If defined, copy in EYAML keys from environment vars
    if [ "${PUPPET_EYAML_PUBKEY}" ] && [ "${PUPPET_EYAML_PUBKEY}" ]; then
	echo "${PUPPET_EYAML_PUBKEY}" > ${PUPPET_KEYSDIR}/public_key.pkcs7.pem
	echo "${PUPPET_EYAML_PRIVKEY}" > ${PUPPET_KEYSDIR}/private_key.pkcs7.pem
    fi
    chmod -R 0400 ${PUPPET_KEYSDIR}/*.pem
    chmod 0500 ${PUPPET_KEYSDIR}

fi

# Create custom facts from params & variables supplied by Scalr, so they're available to hiera
mkdir -p /etc/facter/facts.d/
[ "%puppet_role%" ] && [ "%puppet_role%" != 'no_role' ] && echo "role=%puppet_role%" >> /etc/facter/facts.d/role.txt
echo "puppet_server=${PUPPET_MASTER}" >> /etc/facter/facts.d/puppet_server.txt
echo "puppet_ca_server=${PUPPET_CA}" >> /etc/facter/facts.d/puppet_ca_server.txt
[ "${PUPPET_LOCATION}" ] && echo "project=${PUPPET_LOCATION}" >> /etc/facter/facts.d/location.txt
[ "${PUPPET_PROJECT}" ] && echo "project=${PUPPET_PROJECT}" >> /etc/facter/facts.d/project.txt
[ "${PUPPET_LOCATION}" ] && echo "location=${PUPPET_LOCATION}" >> /etc/facter/facts.d/location.txt

case "%puppet_role%" in
    role::puppet::puppetserver* | role::puppet::puppetca )
	## Bootstrap the puppetserver or CA
	${SCRIPT_PATH}/bootstrap_puppetserver.sh "%puppet_role%"
	;;
    no_role )
	## Enable the puppet service but don't run bootstrap script
	${PUPPET_BINDIR}/puppet resource service puppet ensure=running enable=true
	;;
    * )
	## Bootstrap a puppet node (including puppetdb)
	${SCRIPT_PATH}/bootstrap_agent.sh "%puppet_role%"
	;;
esac
