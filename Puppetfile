forge "https://forgeapi.puppetlabs.com"

# Specify explicit version of modules where needed.

# Below are 3rd-party Puppet Forge modules to satify direct and indirect module dependencies.
mod 'ghoneycutt/common'
mod 'ghoneycutt/hosts'
#mod 'ghoneycutt/nfs'
#mod 'ghoneycutt/ssh'
mod 'ghoneycutt/rpcbind'
mod 'ghoneycutt/types'
mod 'puppetlabs/firewall'
mod 'puppetlabs/concat', '1.2.5'
mod 'puppetlabs/ntp'
#mod 'puppetlabs/vcsrepo'
mod 'puppetlabs/git'
mod 'puppetlabs/stdlib'
mod 'puppetlabs/tomcat'
mod 'puppetlabs/postgresql'
mod 'puppetlabs/puppet_authorization'
mod 'puppetlabs/hocon'
mod 'puppetlabs/inifile'
mod 'puppetlabs/apt'
mod 'puppetlabs/mysql', '3.7.0'
mod 'puppetlabs/apache'
mod 'puppetlabs/java'
mod 'puppetlabs/gcc'
mod 'puppetlabs/ruby'
mod 'puppetlabs/pe_gem'
#mod 'puppetlabs/puppetdb'
#mod 'maestrodev/maven'
mod 'saz/sudo'
mod 'saz/timezone'
mod 'maestrodev/wget'
mod 'nanliu/staging'
mod 'aboe/chrony'
mod 'croddy/make'
mod 'gentoo/portage'
#mod 'deric/accounts'
mod 'herculesteam/augeasproviders_core'
mod 'herculesteam/augeasproviders_sysctl'
mod 'CERNOps/motd'
#mod 'tylerwalts/jdk_oracle'
mod 'stahnma/epel'
#mod 'example42/network'
mod 'example42/stdmod'
#mod 'meltwater/cpan'
mod 'zack/r10k'
#mod 'icinga/icingaweb2'
#mod 'icinga/icinga2'
mod 'thias/postfix'

# Below are 3rd party modules via git
# TODO: revisit ghoneycutt/ssh when PR accepted https://github.com/ghoneycutt/puppet-module-ssh/pull/158
mod 'ghoneycutt/ssh',
  :git => 'https://github.com/westbywest/puppet-module-ssh',
  :commit => '97ec7e25ad724de7b2b284968e2c2840b6aa8158'
# TODO: revisit example42/network module once this version (v3.1.23?) published to forge
mod 'example42/network',
  :git => 'https://github.com/example42/puppet-network',
  :commit => '40e4970c84b6102ed8ba9d40e1b70fb1f075ac84'
# TODO: revisit ghoneycutt/nfs once EL7 updates in Ben's PR published to forge
#mod 'ghoneycutt/nfs',
#  :git => 'https://github.com/ghoneycutt/puppet-module-nfs',
#  :commit => '89bebb1441aff3e151c9d334d489565007dff1c7'
mod 'ghoneycutt/nfs',
  :git => 'https://github.com/westbywest/puppet-module-nfs',
  :commit => '81a4166105a5e7579f19ae6761e5bf07907bf0a9'
# TODO: revisit maestrodev/maven once updates in Ben's PR accepted
mod 'maestrodev/maven',
  :git => 'https://github.com/westbywest/puppet-maven',
  :commit => '15cd44acd960f91143075b608227aa3fe427e934'
# TODO: remove camptocamp/accounts when confirmed it can't be used
#mod 'camptocamp/accounts',
#  :git => 'https://github.com/westbywest/puppet-accounts.git',
#  :commit => '857c61230080aef5c2063b5c74f569bb2b259a9b'
# TODO: revisit deric/accounts once next version published to forge (v1.1.4+)
mod 'deric/accounts',
  :git => 'https://github.com/deric/puppet-accounts.git',
  :commit => 'c5802776fee8bf04dae705d0d2b74089ca2c0b01'
# TODO: revisit tylerwalts/jdk_oracle module once these changes (v1.3.4?) published to forge
#mod 'tylerwalts/jdk_oracle',
#  :git => 'https://github.com/tylerwalts/puppet-jdk_oracle',
#  :commit => '22faf1a7aba2dece47ec9f54a78cd499ddefcab4'
# TODO: revisit theforeman/puppet once next version published to forge (v4.1.1)
#mod 'theforeman/puppet',
#  :git => 'https://github.com/theforeman/puppet-puppet',
#  :commit => '3d96a1dca4367cd27e35be381751735412b03bd9'
mod 'theforeman/puppet',
  :git => 'https://github.com/westbywest/puppet-puppet',
  :commit => 'ccec2e1e79fd65ac2df72bf49c1ef41dbcadfadd'
# TODO: revisit puppetlabs/puppetdb whenever theforeman/puppet is upgraded to support latest version, and Ben's puppetdb PR accecpted
# REF:
mod 'puppetlabs/puppetdb',
  :git => 'https://github.com/westbywest/puppetlabs-puppetdb',
  :commit => '9a2681d0fed36eb2b995adff4f3e4a80439364cb'
# TODO: revisit puppetlabs/vcsrepo whenever and Ben's PR for hg -r option accecpted
# REF:
mod 'puppetlabs/vcsrepo',
  :git => 'https://github.com/westbywest/puppetlabs-vcsrepo',
  :commit => '816606e8944af980f3c2a2b351fa8e42b158167e'
# TODO: revisit hunner/hiera once next version published to forge (v1.3.2)
mod 'hunner/hiera',
  :git => 'https://github.com/hunner/puppet-hiera',
  :commit => '0e3e46624dfa8c99d8944df17cef8d7c7ef13203'
# TODO: revisit icinga/icinga2 once published to forge (v1.0?)
mod 'puppet/icinga2',
  :git => 'https://github.com/Icinga/puppet-icinga2',
  :commit => 'f5fe7405c3d3940aa647dbded3ec68cead9ae588'
# TODO: revisit icinga/icingaweb2 once apprpriate version published to forge (v1.0.7?)
mod 'icinga/icingaweb2',
  :git => 'https://github.com/Icinga/puppet-icingaweb2',
  :commit => '85fcd131ee77c74b4ce0712cfe7e20f1fe8c836c'
# TODO: revisit meltwater/cpan once apprpriate version published to forge (v1.0.2?)
mod 'meltwater/cpan',
  :git => 'https://github.com/meltwater/puppet-cpan',
  :commit => 'f7c627a01c4d6e600a250e7b165fc01a8da678b0'


# Below are NRG custom modules via git.
# Note that current version r10k only understands pulling modules from dedicated git repos.
mod 'nrg/nrgillumos',
  :git => 'git@bitbucket.org:NRGpuppet/puppet_module_nrgillumos.git',
  :ref => '0.1.8'
mod 'nrg/nrgzfs',
  :git => 'https://bitbucket.org/NRGpuppet/puppet_module_nrgzfs.git'
mod 'ozmt/ozmt',
  :git => 'https://bitbucket.org/ozmt/puppet_module_ozmt.git'
mod 'nrg/xnat',
  :git => 'https://bitbucket.org/NRGpuppet/puppet_module_xnat.git'
