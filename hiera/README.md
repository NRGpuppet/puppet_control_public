# NRG Puppet Control Repo Hiera Data

## Overview

This directory contains hiera data for the NRG Puppet control repo, in YAML and encrypted YAML
(EYAML) formats.  These data are valid within the current environment scope, aka the current branch
of this repo.  Specifically, these data are comprised of Puppet modules/classes to run on nodes
being provisioned, and parameters for those modules/classes.

## Hierarchy

*Please read the section below about Hiera merge behavior.*

Hiera allows you to set and override parameters per a hierarchy of data sources you've configured
for hiera.  That is, the directories and files at this path are ranked hierarchy levels, with higher
levels overriding the lower ones.  So, for any parameter defined at multiple levels, the value
assigned at the highest level of hierarchy is that which takes precedence.  Since the levels of
hierarchy represent some property of the node which Puppet is configuring, e.g. operating system,
virtualization platform, hostname, the convention is to arrange the levels of hierarchy with 
highest = most specific (e.g. FQDN) down to lowest = most generic (e.g. common, all nodes).  In
addition, levels are arranged so that secure EYAML takes precendence over unencrypted YAML.

The hierarchy for the active Puppetserver is that defined in its /etc/puppetlabs/code/hiera.yaml.
Note this hierarchy is itself under Puppet control too, and the contents of that file will be
dictated by one of the following:

- Code in profile::manifests::puppet::hiera class (which is run on the Puppetserver)
- Hiera parameter key hiera::hierarchy, e.g. in common.yaml

The code in the profile::manifests::puppet::hiera is there to bootstrap a minimal hiera hierarchy
sufficient for the Puppetserver to complete its first puppet agent fun, i.e. before hiera is fully
configured on it.  After which, it will apply the hierarchy retrieved from hiera itself.

This is the hierarchy at the time of writing, ranked from highest (1) on down:

1. secure.eyaml
1. fqdn/{fqdn}.yaml
1. role/{role}.yaml
1. project/{project}_secure.eyaml
1. project/{project}{operatingsystem}{operatingsystemmajrelease}.yaml
1. project/{project}{operatingsystem}.yaml
1. project/{project}.yaml
1. virtual/{virtual}_secure.eyaml
1. virtual/{virtual}{operatingsystem}{operatingsystemmajrelease}.yaml
1. virtual/{virtual}{operatingsystem}.yaml
1. virtual/{virtual}.yaml
1. location/{location}_secure.eyaml
1. location/{location}{operatingsystem}{operatingsystemmajrelease}.yaml
1. location/{location}{operatingsystem}.yaml
1. location/{location}.yaml
1. operatingsystem/{operatingsystem}{operatingsystemmajrelease}.yaml
1. operatingsystem/{operatingsystem}.yaml
1. common.yaml

## Merge Behavior

At the time of writing, this hiera hierarchy is configured for "deeper" merging behavior, meaning 
that hiera() lookups done explicitly in Puppet code, or in recursive lookups in hiera itself, will
return hash keys and arrays merged across all levels of hierarcy (but with specific keys defined
at multiple levels still overridden as described above).

Please refer to:
https://docs.puppetlabs.com/hiera/3.0/lookup_types.html#deep-merging-in-hiera

*However*, this merge behavior does not occur on the implicit hiera lookups performed on Puppet
class parameters, which only do native merging, whereby hashes and arrays are NOT merged across
all levels.  This inconsistency is a limitation of Puppet.

Please refer to:
https://docs.puppetlabs.com/hiera/3.0/lookup_types.html#native-merging

## Custom Facts

You can query built-in facts available to hiera/Puppet via "facter -p" on the node being
provisioned.  Custom facts, e.g. location, can be specified for nodes in the following
ways:

* Via a plugin delivered to the node by a custom module
* Locally on the node in /etc/facter/facts.d

For values defined in hiera that you also want to invoke in the hierarchy, e.g. "project" or
"location," note that you will need to have the environment-wide manifest in manifests/site.pp
do a hiera() lookup on that value and then set a top-level variables of the same name.  The
example below does just that, first verifying that $::project isn't already set (i.e. as a
custom fact ), and also setting a default value if the hiera() lookup returns nothing:

```
if (! $::project) {
  $project = hiera('project', 'defaultproject')
}
```

## Debugging Hiera

You can do hiera lookups at the command line on the active Puppetserver, specifying the
environment and relevant facts in command line options.  Note the use of the --hash and --array
options to format the retrieved data correctly.

```
# hiera timezone::timezone ::environment=production ::virtual=xen
# hiera --hash accounts::users ::environment=production ::operatingsystem=CentOS
# hiera --array classes ::environment=dev ::project=myproject
```

You'll need to run 'hiera' under sudo/root privileges to access the keys needed to unlock EYAML
values.

## References

Hiera 3: Using Hiera With Puppet
https://docs.puppetlabs.com/hiera/3.0/puppet.html

Hiera 3: Complete Example
https://docs.puppetlabs.com/hiera/3.0/complete_example.html

Hiera 3: Lookup Types
https://docs.puppetlabs.com/hiera/3.0/lookup_types.html

Encrypt Your Data Using Hiera-Eyaml
https://puppetlabs.com/blog/encrypt-your-data-using-hiera-eyaml

Debugging Hiera
https://puppetlabs.com/blog/debugging-hiera
